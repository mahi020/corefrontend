import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';

class Slider extends Component {
  render() {
    return (
      <Carousel showStatus={false} autoPlay={true}>
      <div>
      <img src="images/about.jpg" />
      <p className="legend" style={{ cursor:'pointer'}}>Legend 1</p>
      </div>
      <div>
      <img src="images/blog-1.jpg" />
      <p className="legend" style={{ cursor:'pointer'}}>Legend 2</p>
      </div>
      <div>
        <img src="images/blog-2.jpg" />
        <p className="legend" style={{ cursor:'pointer'}}>Legend 3</p>
      </div>
     </Carousel>


    );
  }
}

export default Slider;
