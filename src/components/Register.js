import React, { Component } from 'react';
//import axios from 'axios';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { registerUser } from '../actions/authActions';
import { withRouter , Link} from 'react-router-dom';

class Register extends Component {
  constructor() {
    super();
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      country: ['Australia','Bangladesh','Canada', 'Denmark','England', 'Finland','Germany','Hungary','Italy', 'India', 'Norway','Paraguay', 'Russia', 'Sweden', 'USA'],
      address: '',
      zip: '',
      errors: {}
    };
    
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeDropdown = this.onChangeDropdown.bind(this);
    this.country='';
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onChangeDropdown(e)
  {
    //console.log(e.target.value);
    this.country = e.target.value;
  }
  componentWillReceiveProps(nextProps)
  {
      if(nextProps.errors)
      {
        this.setState({ errors: nextProps.errors });
      }

  }
  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/');
    }
  }

  onSubmit(e) {
    e.preventDefault();
    
    if(this.country.length==0)
    {
      this.country = this.state.country[0];
    }

    const newUser = {

      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      password: this.state.password,
      address: this.state.address,
      country: this.country,
      zip: this.state.zip
    };
    //console.log(newUser);
    this.props.registerUser(newUser, this.props.history);
    //console.log(this.props);
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="register">
        <div className="container">
        <br/> <br/> <br/>
          <div className="row">
          <div className="col-md-12">
            <div className="row">
                <div className="col-md-5 register-left">
                <br/> <br/> <br/>
                    <h3> Join Us </h3>
                    <h5>
                        Buy Anything.
                    </h5>
                    <h5>
                    Upload Your Product and Sell.
                    </h5>
                    <Link to="/about" className="btn btn-danger mt-2"> About Us </Link>
                </div>
                <div className="col-md-7 register-right">
                  <h2> Register Here </h2>
                  <form noValidate onSubmit={this.onSubmit} className="register-form">
                <div className="form-group">
                  <input
                    type="email"
                    className={classnames('form-control form-control-lg', {
                      'is-invalid': errors.email
                    })}
                    placeholder="Email Address"
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                  {errors.email && (
                    <div className="invalid-feedback">{errors.email}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    className={classnames('form-control form-control-lg', {
                      'is-invalid': errors.password
                    })}
                    placeholder="Password"
                    name="password"
                    value={this.state.password}
                    onChange={this.onChange}
                  />
                  {errors.password && (
                    <div className="invalid-feedback">{errors.password}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames('form-control form-control-lg', {
                      'is-invalid': errors.firstname
                    })}
                    placeholder="First Name"
                    name="firstname"
                    value={this.state.firstname}
                    onChange={this.onChange}
                  />
                  {errors.firstname && (
                    <div className="invalid-feedback">{errors.firstname}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames('form-control form-control-lg', {
                      'is-invalid': errors.lastname
                    })}
                    placeholder="Last Name"
                    name="lastname"
                    value={this.state.lastname}
                    onChange={this.onChange}
                  />
                  {errors.lastname && (
                    <div className="invalid-feedback">{errors.lastname}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames('form-control form-control-lg', {
                      'is-invalid': errors.address
                    })}
                    placeholder="Address"
                    name="address"
                    value={this.state.address}
                    onChange={this.onChange}
                  />
                  {errors.address && (
                    <div className="invalid-feedback">{errors.address}</div>
                  )}
                </div>
                <div className="row">
                <div className="col-md-5 mb-3">
                  <label>Country</label>
                    <select className="custom-select d-block w-100" defaultValue="0" name="country" required onChange={this.onChangeDropdown}>
                      {
                        this.state.country.map((item,index)=>{

                          return (<option key={index}> { item } </option>);

                        })
                      }
                    </select>
                </div>

                <div className="col-md-4 mb-3">
                  <label>Zip Code</label>
                  <input type="text" 
                  className={classnames('form-control', {
                      'is-invalid': errors.zip
                    })} 
                    name="zip" required
                    value={this.state.zip}
                    onChange={this.onChange}
                    />
                  {errors.zip && (
                    <div className="invalid-feedback">{errors.zip}</div>
                  )}
                </div>

            </div>

                <button className="btn btn-info btn-block mt-4">Sign Up</button>
            </form>
                </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(mapStateToProps , { registerUser })(withRouter(Register));
