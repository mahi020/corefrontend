import React, { Component } from 'react';


class Table1 extends Component {
  render() {
    return (
      <div>
        <main className="container pt-5">
        <div className="card mb-5">
            <div className="card-header">Fearures</div>
            <div className="card-block p-0">
                <table className="table table-bordered table-sm m-0">
                    <thead className="">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Registration Date</th>
                            <th>E-mail address</th>
                            <th>Premium Plan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input"></input>
                                    <span className="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>John Lilki</td>
                            <td>September 14, 2013</td>
                            <td>jhlilk22@yahoo.com</td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <td>
                                <label className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input"></input>
                                    <span className="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>John Lilki</td>
                            <td>September 14, 2013</td>
                            <td>jhlilk22@yahoo.com</td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <td>
                                <label className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input"></input>
                                    <span className="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>John Lilki</td>
                            <td>September 14, 2013</td>
                            <td>jhlilk22@yahoo.com</td>
                            <td>No</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="card-footer p-0">
                <nav aria-label="...">
                    <ul className="pagination justify-content-end mt-3 mr-3">
                        <li className="page-item disabled">
                            <span className="page-link">Previous</span>
                        </li>
                        <li className="page-item"><a className="page-link" href="#">1</a></li>
                        <li className="page-item active">
                            <span className="page-link">2<span className="sr-only">(current)</span>
                            </span>
                        </li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item">
                            <a className="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        
    </main>
      </div>
    );
  }
}

export default Table1;