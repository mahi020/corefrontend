import React, { Component } from 'react';
//import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import classnames from 'classnames';
import { sendRecoveryEmail, sendCode, sendNewPassword } from '../actions/authActions';
import { withRouter } from 'react-router-dom';

class AccountRecovery extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      code:'',
      newPassword: '',
      pass: true,
      e: true,
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.onSubmitCode = this.onSubmitCode.bind(this);
    this.onSubmitNewPassword = this.onSubmitNewPassword.bind(this);
    
  }

  onSubmitEmail(e) {
    e.preventDefault();
    const userData = {
      email: this.state.email,
    };
    //this.props.loginUser(userData, this.props.history);
    //this.setState({ e: false });
    //this.setState({ pass: true });
    this.props.sendRecoveryEmail(userData);
    //console.log(this.state);
  }
  onSubmitCode(e) {
    e.preventDefault();

    const userData = {
      code: this.state.code
    };
    //this.sendRecoveryEmail(userData);
    this.props.sendCode(userData);
    //this.setState({ pass: false });
    //console.log(this.state);
    //this.props.loginUser(userData, this.props.history);
  }

  onSubmitNewPassword(e) {
    e.preventDefault();

    const userData = {
      email: this.state.email,
      newPassword: this.state.newPassword
    };
    console.log(this.state);
    this.props.sendNewPassword(userData);
    //this.props.loginUser(userData, this.props.history);

  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/');
    }
  }

  componentWillReceiveProps(nextProps)
  {
      if(nextProps.auth.inAuthenticated)
      {
        this.props.history.push('/');
      }
      if(nextProps.errors)
      {
        this.setState({ errors: nextProps.errors });
      }
      console.log(nextProps);
      if(nextProps.auth)
      {
        if(nextProps.auth.e== false)
        {
          this.setState({e: nextProps.auth.e});
        }
        if(nextProps.auth.pass==false)
        {
          this.setState({pass: nextProps.auth.pass});
        }
        
      }

  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    let email = (

      <form className="col-12 mt-3" onSubmit={this.onSubmitEmail}>
            <div className="form-group">
                  <input
                    type="email"
                    className={classnames('form-control', {
                      'is-invalid': errors.email
                    })}
                    placeholder="Email Address"
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                  {errors.email && (
                    <div className="invalid-feedback">{errors.email}</div>
                  )}
                </div>
                <button type="submit" className="btn btn-info btn-block mt-4 mb-5">
                    Send
              </button>
            </form>


    );
    let code = (

      <form className="col-12 mt-3" onSubmit={this.onSubmitCode}>
            <div className="form-group">
                  <input
                    type="text"
                    className={classnames('form-control', {
                      'is-invalid': errors.code
                    })}
                    placeholder="Enter The Verification Code"
                    name="code"
                    value={this.state.code}
                    onChange={this.onChange}
                  />
                  {errors.code && (
                    <div className="invalid-feedback">{errors.code}</div>
                  )}
                </div>
                <button type="submit" className="btn btn-info btn-block mt-4 mb-5">
                    Next
              </button>
            </form>


    );

    let newPassword = (

      <form className="col-12 mt-3" onSubmit={this.onSubmitNewPassword}>
            <div className="form-group">
                  <input
                    type="password"
                    className={classnames('form-control', {
                      'is-invalid': errors.newPassword
                    })}
                    placeholder="Enter New Password"
                    name="newPassword"
                    value={this.state.newPassword}
                    onChange={this.onChange}
                  />
                  {errors.newPassword && (
                    <div className="invalid-feedback">{errors.newPassword}</div>
                  )}
                </div>
                <button type="submit" className="btn btn-info btn-block mt-4 mb-5">
                    Next
              </button>
            </form>


    );
    console.log(this.state.e);
    console.log(this.state.pass);

    return (
      
      <div className="login">
      <br/> <br/> <br/>
      <div className="container">
        <div className="modal-dialog text-center">
        <div className="col-sm-8 main-section">
         <div className="modal-content">
            <div className="col-12 user-img">
                <img src="https://res.cloudinary.com/mahi020/image/upload/v1553782693/User.png" alt="userImg"/>
            </div>
            <h4 style={{color: 'white'}}>Account Recovery</h4>
            {
                this.state.e ? email: this.state.pass? code: newPassword
            }
         </div>
         </div>
        </div>
        </div>  
      </div>
    );
  }
}

const mapStateToProps = state =>({
    auth:state.auth,
    errors:state.errors
});

export default connect(mapStateToProps, { sendRecoveryEmail, sendCode, sendNewPassword })(withRouter(AccountRecovery));
