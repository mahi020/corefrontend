import React from 'react'
import axios from 'axios';
import StripeCheckout from 'react-stripe-checkout';
import jwt_decode from 'jwt-decode';
import STRIPE_PUBLISHABLE from './constants/stripe';
import PAYMENT_SERVER_URL from './constants/server';
import Swal from 'sweetalert2';

const CURRENCY = 'EUR';

const fromEuroToCent = amount => amount * 100;

const successPayment = data => {
  Swal.fire(
    'Payment Successful',
    'Thanks for the shopping on our E-Shop',
    'success'
  );
  setTimeout(function(){
    window.location.href = '/';
  },1000);
  
};

const errorPayment = data => {
  Swal.fire({
    type: 'error',
    title: 'Oops...',
    text: 'Payment Error'
  });
};

const onToken = (amount, description) => token =>
  axios.post(PAYMENT_SERVER_URL,
    {
      description,
      source: token.id,
      currency: CURRENCY,
      amount: fromEuroToCent(amount)
    })
    .then(res=>{
      let decoded = jwt_decode(localStorage.jwtToken);
      console.log(decoded.id);
      let cart = localStorage.cart;
      cart = JSON.parse(cart);
      let address = localStorage.getItem('address');
      let country = localStorage.getItem('country');
      let zip = localStorage.getItem('zip');
      console.log(res.data.success);
      let newData = {
        cart: cart,
        userId: decoded.id,
        total: res.data.success.amount,
        address,
        country,
        zip
      };
      axios.post('/orders', newData).then(res=>{

        console.log(res);
        localStorage.removeItem('cart');
        localStorage.removeItem('address');
        localStorage.removeItem('country');
        localStorage.removeItem('zip');
        successPayment(res);
        
      }).catch(err=>{
        console.log(err);
    });    
    
      //cart = JSON.parse(cart);
      //console.log(cart);
      
    })
    .catch(errorPayment);

const Checkout = ({ name, description, amount }) =>
  <StripeCheckout
    name={name}
    description={description}
    amount={fromEuroToCent(amount)}
    token={onToken(amount, description)}
    currency={CURRENCY}
    stripeKey={STRIPE_PUBLISHABLE}
  />

export default Checkout;