import React, { Component } from 'react';


class NotFound extends Component {
  render() {
    return (
      
        <div className="mt-5">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h3 className="text-center">Not Found</h3>

            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default NotFound;
