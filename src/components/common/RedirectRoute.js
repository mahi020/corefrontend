import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const RedirectRoute = ({component: Component, ...rest}) =>{

  let f = false;
  return(
    <Route
      {...rest}
      render={(props)=>
        f? <Component {...props} />: <Redirect
        to={`/${props.history.goBack()}`}
      />
      }
    />
  );
};

export default RedirectRoute;
