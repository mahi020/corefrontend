import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const AdminRoute = ({component: Component, auth, ...rest}) =>{
  console.log(auth);
  return(
    <Route
      {...rest}
      render={(props)=>
        auth.isAdmin===true && auth.isAuthenticated===true? <Component {...props} />: <Redirect
        to={`/${props.history.goBack()}`}
      />
      }
    />
  );
};

const mapStateToProps = state => ({

  auth: state.auth

});
export default connect(mapStateToProps)(AdminRoute);
