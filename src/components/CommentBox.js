import React, { Component } from 'react';
import { connect } from 'react-redux';
import StarRatings from 'react-star-ratings';
import { setRating } from '../actions/productActions';
import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';

class CommentBox extends Component {

    constructor()
    {
        super();
        this.state = {

            comment:'',
            rating:0
        };
        this.changeRating = this.changeRating.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onClick = this.onClick.bind(this);
   
    }
    handleChange(event) {
        this.setState({comment: event.target.value});
        //console.log(this.state.comment);
      }
    changeRating(newRating) {

        //console.log(newRating);
        if(this.props.product.canRate)
        {
            //let rate = this.props.product.item.rating;
            //console.log(this.props.product.item);
            //console.log(rate);
            this.setState({rating: newRating});
        }
    
    }
    onClick()
    {
        console.log(this.state.comment);
        console.log(this.state.rating);

        if(this.state.rating===0 && this.state.comment==='')
        {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Rating & Review Both Fields Can not be Empty!'
              });
        }
        else
        {
            this.props.setRating(this.props.product.item._id, this.state.rating, this.state.comment, this.props.auth.user.id, this.props.history);
        }
       
        
    }
    
    render(){
    
        let comments = this.props.product.comments;

        let commentBox = (
        
        <div className="m-5">
        <div className="col-md-10 col-md-offset-2">
        <h5 className="mb-3">Give Your Review Here</h5>
        {this.props.product.isSeller? '':(<div className="mb-4">
              <StarRatings
                rating={this.state.rating}
                starRatedColor= 'rgba(255,195,0,0.8)'
                changeRating={this.changeRating}
                name='rating'
                starDimension="24px"
                starSpacing="4px"
                />
                                    
            </div>)
        }    
            <div className="card card-info">
                <div className="card-block">
                    <textarea placeholder="Write your comment here!" className="pb-cmnt-textarea" onChange={this.handleChange} value = {this.state.comment}></textarea>
                </div>
                <button className="btn btn-info pull-right" type="button" onClick={this.onClick}>Share</button>
            </div>
        </div>
        </div>

    );
    return(

        <div>

            { this.props.product.canRate | this.props.product.isSeller ? commentBox:'' }
        
        </div>
        

    );
    }

}

const mapStateToProps = state =>({
    auth:state.auth,
    product: state.product
});

export default connect(mapStateToProps, { setRating })(withRouter(CommentBox));
