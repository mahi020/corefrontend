import React, { Component } from 'react';


class Table3 extends Component {
  render() {
    return (
      <div>
        <main className="container pt-5">
        
        <table className="table  table-sm">
            <thead className="table-info">
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Registration Date</th>
                    <th>E-mail address</th>
                    <th>Premium Plan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <label className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input"></input>
                            <span className="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td>John Lilki</td>
                    <td>September 14, 2013</td>
                    <td>jhlilk22@yahoo.com</td>
                    <td>No</td>
                </tr>
                <tr>
                    <td>
                        <label className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input"></input>
                            <span className="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td>John Lilki</td>
                    <td>September 14, 2013</td>
                    <td className="table-danger">jhlilk22@yahoo.com</td>
                    <td>No</td>
                </tr>
                <tr>
                    <td>
                        <label className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input"></input>
                            <span className="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td>John Lilki</td>
                    <td>September 14, 2013</td>
                    <td>jhlilk22@yahoo.com</td>
                    <td>No</td>
                </tr>
            </tbody>
        </table>
    </main>
      </div>
    );
  }
}

export default Table3;