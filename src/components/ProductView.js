import React, { Component } from 'react';
import NiceImage from './NiceImage';
import { getAllProducts } from '../actions/productActions';
import { connect } from 'react-redux';
import AllProductsView from './AllProductsView';
import Footer from './Footer';

class ProductView extends Component {
	 
		componentDidMount()
		{
			this.props.getAllProducts();
		}
 	render() {
		//const { allProducts } = this.props.product;
		const styles = {
			color:'#1e90ff',
		 };
	return (
		<div className="container-fluid pr-0 pl-0">
		 <NiceImage/>
		<div style={{ marginTop: '-5%'}}>
			<center><h2 style={styles}>All Products</h2>
				<p className="para-1"> Buy Any Product of Your Choice. </p>
			</center>
			<br/>
			<AllProductsView/>
			<br/><br/><br/>
			<Footer/>
		</div>
			
		 
	    </div> 
		  
      );
    }
  }
  const mapStateToProps = state =>({

		product: state.product

	});
  export default connect(mapStateToProps, { getAllProducts })(ProductView);
  