import React, { Component } from 'react';


class Table2 extends Component {
  render() {
    return (
      <div>
        <main className="container pt-5">
        
        <table className="table table-bordered table-definition mb-5">
            <thead className="table-warning ">
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Registration Date</th>
                    <th>E-mail address</th>
                    <th>Premium Plan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <label className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input"></input>
                            <span className="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td>John Lilki</td>
                    <td>September 14, 2013</td>
                    <td>jhlilk22@yahoo.com</td>
                    <td>No</td>
                </tr>
                <tr>
                    <td>
                        <label className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input"></input>
                            <span className="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td>John Lilki</td>
                    <td>September 14, 2013</td>
                    <td>jhlilk22@yahoo.com</td>
                    <td>No</td>
                </tr>
                <tr>
                    <td>
                        <label className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input"></input>
                            <span className="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td>John Lilki</td>
                    <td>September 14, 2013</td>
                    <td>jhlilk22@yahoo.com</td>
                    <td>No</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th colspan="4">
                        <button className="btn btn-primary float-right">Add User</button>
                        <button className="btn btn-default">Approve</button>
                        <button className="btn btn-default">Approve All</button>
                    </th>
                </tr>
            </tfoot>
        </table>
        
    </main>
      </div>
    );
  }
}

export default Table2;