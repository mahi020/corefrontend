import React, { Component } from 'react';
import { getProfileInfo } from '../../actions/profileActions';
import { connect } from 'react-redux';
import AdminSidebar from './AdminSidebar';

class Me extends Component {

    componentDidMount()
    {
        this.props.getProfileInfo(this.props.auth.user.id);
        //console.log("id is: ",this.props.auth.user.id);
        console.log(this.props);
    }
    render(){
        const { profile } = this.props.profile;
        
        return (
            <div className="container-fluid mt-2 p-0" id="main">
            <br/> <br/>
            <AdminSidebar/>
            <div className="row mediaMargin">
              <div className="col-sm-12 mt-5">
              <h3>Admin Profile</h3>
                      <div className="table-responsive">
                          <table className="table table-striped">
                              <tbody>
                                  
                              <tr>
                                      
                                <th>First Name</th>
                                <td>{ profile.firstname }</td>    
                                      
                              </tr>
                              
                              <tr>
                                      
                                <th>Last Name</th>
                                <td>{ profile.lastname }</td>    
                                      
                              </tr>

                              <tr>
                                      
                                <th>Email</th>
                                <td>{ profile.email }</td>    
                                            
                              </tr>
                                    

                              </tbody>
                          </table>
                      </div>
                  </div>
            
            </div>
              
          
              <hr/>
  
  
          </div>
        );
    }

}

const mapStateToProps = state =>({
    auth:state.auth,
    profile: state.profile
});

export default connect(mapStateToProps, { getProfileInfo })(Me);
