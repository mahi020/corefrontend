import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getASingleProduct } from '../../actions/privateActions';

class ShowAProduct extends Component {
  
  constructor(){
    super();
    this.state = {
      title:'',
      desc:'',
      category:'',
      price:'',
      quantity:'',
      approved:'',
      image:''
    };
   
  }
  componentDidMount()
  {
      this.props.getASingleProduct(this.props.match.params.id);
  }
  componentWillReceiveProps(nextProps)
  {
    this.setState({
      title: nextProps.private.singleProduct.title,
      desc: nextProps.private.singleProduct.desc,
      category: nextProps.private.singleProduct.category,
      price: nextProps.private.singleProduct.price,
      quantity: nextProps.private.singleProduct.quantity,
      approved: nextProps.private.singleProduct.approved,
      image: nextProps.private.singleProduct.image
    });
  }
  
  render() {
      
      const { singleProduct } = this.props.private;

      
      return (
          
        <div className="colorlib-shop mt-5">
        <div className="container mt-5">
        <br/> <br/> <br/>
          <div className="row row-pb-lg">
            <div className="col-md-10 col-md-offset-1">
              <div className="product-detail-wrap">
                <div className="row">
                  <div className="col-md-5">
                    <div className="product-entry">
                      <div className="product-img">
                        <img src={ this.state.image }/>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-7">
                    <h3> Product Details </h3>
                    <br/> 
                    <div className="form-group">
                      <label>Title</label>
                      <input type="text" className="form-control" id="title" name="title" value={this.state.title} disabled="disabled"/>
                    </div>

                    <div className="form-group">
                      <label>Description</label>
                      <textarea className="form-control" name="desc" id="desc" rows="3" value={this.state.desc} disabled="disabled"></textarea>
                    </div>


                    <div className="form-group">
                      <label>Category</label>
                      <input type="text" className="form-control" id="category" name="category" value={this.state.category} disabled="disabled"/>
                    </div>

                    <div className="form-group">
                      <label>Price</label>
                      <input type="text" className="form-control" id="price" name="price" value={this.state.price} disabled="disabled"/>
                    </div>

                    <div className="form-group">
                      <label>Quantity</label>
                      <input type="text" className="form-control" id="quantity" name="quantity" value={this.state.quantity} disabled="disabled"/>
                    </div>

                    

                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
  
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  private: state.private

});

export default connect(mapStateToProps, { getASingleProduct })(ShowAProduct);
  