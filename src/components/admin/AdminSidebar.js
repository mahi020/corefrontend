import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AdminSidebar extends Component {

    
    render(){
      const styles = {
        color:'#FFFFFF'
     };
        return (
            <div className="sidebar pl-0"> 
                <ul className="nav flex-column text-center" id="border">
                <br/> <br/>
                <li className="nav-item">
                    <Link className="nav-link" to="/admin/me" style={styles}>Home</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/admin/home" style={styles}>All Orders</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/admin/allUsers" style={styles}>All Users</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/admin/allProducts" style={styles}>All Products</Link>
                  </li>
                  
                </ul>
            </div>

        );
    }

}

export default AdminSidebar;
