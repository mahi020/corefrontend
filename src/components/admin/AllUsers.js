import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllUsers, setModerator, removeFromModerator } from '../../actions/privateActions';
import { withRouter } from 'react-router-dom';
import AdminSidebar from './AdminSidebar';

class AllUsers extends Component {

    constructor(){
        super();
        this.showProducts = this.showProducts.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onClickRemove = this.onClickRemove.bind(this);
    }
    componentDidMount(){

      this.props.getAllUsers();
      
    }
    onClickRemove(id){

      //console.log("On REmove");
      this.props.removeFromModerator(id, this.props.history);

    }
    onClick(id){

      this.props.setModerator(id, this.props.history);

    }
    
    showProducts(prod){

        let table = []
        for (let i = 0; i < prod.length; i++) 
        {
            let children = []
            if(this.props.auth.user.id !== prod[i]._id)
            {
              children.push(this.productComponent(prod[i]));             
              table.push(children);
            }
            
        }
        return table

    }
    productComponent(child){
        //console.log(child);
        const makeModerator = (<button className="btn btn-info" onClick={ ()=>{ this.onClick(child._id) } }>Make Moderator</button>);
        const removeModerator = (<button className="btn btn-info" onClick={ ()=>{ this.onClickRemove(child._id) } }>Remove From Moderator</button>);
        return (
            <tr key={child._id}>
                <td> { child.firstname } </td>
                <td> { child.lastname } </td>
                <td> { child.email } </td>
                <td> { child.is_moderator? removeModerator: makeModerator} </td>
            </tr>
        );
    }


    render() {

      const { allUsers } = this.props.private;
      return (
          
        <div className="container-fluid mt-2 p-0" id="main">
          <br/> <br/>
          <AdminSidebar/>  
          <div className="row mediaMargin">
            <div className="col-sm-12 mt-5">
                    <div className="table-responsive">
                        <table className="table table-striped">
                            <thead className="thead-inverse">
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>E-mail</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                               { this.showProducts(allUsers) }
                            </tbody>
                        </table>
                    </div>
                </div>
          
          </div>
            
        
            <hr/>


        </div>
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  private: state.private,
  auth: state.auth

});

export default connect(mapStateToProps, { getAllUsers, setModerator, removeFromModerator })(withRouter(AllUsers));
  