import React, { Component } from 'react';

class TeamDesign extends Component {
  render() {
    return (
      <div>
        <section id="team" className="pb-5">
        <div className="container">
        <br/> <br/> <br/>
        <h5 className="section-title h1">OUR TEAM</h5>
        <div className="row">
        
            <div className="col-xs-12 col-sm-6 col-md-6">
                <div className="image-flip" onTouchStart="this.classList.toggle('hover');">
                    <div className="mainflip">
                        <div className="frontside">
                            <div className="card">
                                <div className="card-body text-center">
                                    <p><img className=" img-fluid" src="https://res.cloudinary.com/mahi020/image/upload/v1552673266/eqwz8cczmuwmljwq1odc.jpg" alt="card image"></img></p>
                                    <h4 className="card-title">Md. Mahir Hussain Mahi</h4>
                                    <h5 className="card-text">Developer</h5>
                                </div>
                            </div>
                        </div>
                        <div className="backside">
                            <div className="card">
                                <div className="card-body text-center mt-4">
                                    <h4 className="card-title">Md. Mahir Hussain Mahi</h4>
                                    <h5 className="card-text">ID: 151-115-020</h5>
                                    <h5 className="card-text">Metropolitan University Sylhet, Bangladesh</h5>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="col-xs-12 col-sm-6 col-md-6">
                <div className="image-flip" onTouchStart="this.classList.toggle('hover');">
                    <div className="mainflip">
                        <div className="frontside">
                            <div className="card">
                                <div className="card-body text-center">
                                    <p><img className=" img-fluid" src="https://res.cloudinary.com/mahi020/image/upload/v1552675556/Shuvo.png" alt="card image"></img></p>
                                    <h4 className="card-title">Rahul Bhowmik Shuvo</h4>
                                    <h5 className="card-text">Developer</h5>
                                </div>
                            </div>
                        </div>
                        <div className="backside">
                            <div className="card">
                                <div className="card-body text-center mt-4">
                                <h4 className="card-title">Rahul Bhowmik Shuvo</h4>
                                    <h5 className="card-text">ID: 151-115-030</h5>
                                    <h5 className="card-text">Metropolitan University Sylhet, Bangladesh</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>
      </div>
    );
  }
}

export default TeamDesign;
