import React, { Component } from 'react';
//import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { loginUser } from '../actions/authActions';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();

    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData, this.props.history);

  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/');
    }
  }

  componentWillReceiveProps(nextProps)
  {
      if(nextProps.auth.inAuthenticated)
      {
        this.props.history.push('/');
      }
      if(nextProps.errors)
      {
        this.setState({ errors: nextProps.errors });
      }

  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="login">
      <br/> <br/> <br/>
      <div className="container">
        <div className="modal-dialog text-center">
        <div className="col-sm-8 main-section">
         <div className="modal-content">
            <div className="col-12 user-img">
                <img src="https://res.cloudinary.com/mahi020/image/upload/v1553782693/User.png" alt="userImg"/>
            </div>
            <h4 style={{color: 'white'}}>Sign in to your account</h4>

            <form className="col-12 mt-3" onSubmit={this.onSubmit}>

            <div className="form-group">
                  <input
                    type="email"
                    className={classnames('form-control', {
                      'is-invalid': errors.email
                    })}
                    placeholder="Email Address"
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                  {errors.email && (
                    <div className="invalid-feedback">{errors.email}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    className={classnames('form-control', {
                      'is-invalid': errors.password
                    })}
                    placeholder="Password"
                    name="password"
                    value={this.state.password}
                    onChange={this.onChange}
                  />
                  {errors.password && (
                    <div className="invalid-feedback">{errors.password}</div>
                  )}
                </div>
                <button type="submit" className="btn btn-info btn-block mt-4">
                    Login
              </button>

            </form>
            <div className="col-12 forgot">
                <Link to="/accountRecovery" style={{color: 'white'}}>Forgot Password?</Link>
            </div>

         </div>
         </div>
        </div>
        </div>  
      </div>
    );
  }
}

const mapStateToProps = state =>({
    auth:state.auth,
    errors:state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);
