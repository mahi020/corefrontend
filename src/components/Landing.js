import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
//import NiceImage from './NiceImage';
//import Footer from './Footer';

class Landing extends Component {
  render() {
    return (
      <div>
      <div className="landing">
        <div className="dark-overlay landing-inner text-light">
          <div className="container">
            <div className="row">
              <div className="col-md-12 text-center">
                <h1 className="display-3 mb-4">First Project</h1>
                <p className="lead">
                  {' '}
                  Buy and Sell Products in Our Web application
                </p>
                <hr />
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default Landing;
