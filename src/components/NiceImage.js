import React, { Component } from 'react';
//import ProductView from './ProductView';

class NiceImage extends Component {
   
    render() {
      const styles = {
        color:'#1e90ff',
        display: 'inline', 
        wordpacing: '10px'
     };
      return (

      <React.Fragment>
        <div id="colorlib" className="colorlib" data-stellar-background-ratio="0.5">
            <div className="container">
            
            <div className="row">
              <div className="col-md-7">

                <h3 style={styles}>If You Have Never Fallen in Love at First Sight.</h3>
                <br/>
                <h3 style={styles}>You're Shopping on the Wrong Site.</h3>
                
                
            </div>
            </div>
            </div>
        </div>
        </React.Fragment>
      );
    }
  }
  
  export default NiceImage;
  



