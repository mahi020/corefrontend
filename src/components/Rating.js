import React, { Component } from 'react';
import StarRatings from 'react-star-ratings';
import { connect } from 'react-redux';
import { setRating } from '../actions/productActions';

class Rating extends Component {

    constructor(){
        super();
        this.rating = 0;
        this.changeRating = this.changeRating.bind(this);
    }

    changeRating( newRating, name ) {

        console.log(newRating);
        if(this.props.product.canRate)
        {
            this.props.setRating(this.props.product.item._id, newRating, this.props.auth.user.id);
            /*this.setState({
                rating: newRating
              });
            */
           let rate = this.props.product.item.rating;
           console.log(this.props.product.item);
           console.log(rate);
           //this.setState({rating: rate});
        }
        
  }
  render() {

    this.rating = this.props.product.item.rating;
    
    return (

        
        <div className="m-1 disableButton">
        <StarRatings
          rating={this.rating}
          starRatedColor= 'rgba(255,195,0,0.8)'
          changeRating={this.changeRating}
          name='rating'
          starDimension="30px"
          starSpacing="6px"
        />
         
        </div>

    );
  }
}
const mapStateToProps = state =>({

    product: state.product,
    auth: state.auth
  
  });
  

export default connect(mapStateToProps, { setRating })(Rating);
