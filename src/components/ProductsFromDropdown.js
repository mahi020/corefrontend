import React, { Component } from 'react';
import { connect } from 'react-redux';
import PaginationComponent from "react-reactstrap-pagination";
import { getDropdownProducts, addActivePage } from '../actions/productActions';
import { withRouter } from 'react-router-dom';

class ProductsFromDropdown extends Component {
  
    constructor(){

        super();
        this.handleSelected = this.handleSelected.bind(this);
        this.onClicked = this.onClicked.bind(this);
        this.showProducts = this.showProducts.bind(this);
  
      }
      handleSelected(selectedPage) {
          
          this.props.addActivePage(selectedPage);
          this.props.getDropdownProducts(selectedPage, this.props.product.category);    
      }
      componentDidMount()
      {
          let category = this.props.match.params.category;
          console.log(category);
          this.props.getDropdownProducts(this.props.product.activePage, category);
      }
      onClicked(productId){
          //this.props.addAProductToCart(this.props.product.cart, prod);
          console.log("Clicked..");
          this.props.history.push(`/productDetails/${productId}`);
  
      }
      showProducts(prod){
  
          let table = []
          let k=0;
          for (let i = 0; i < prod.length/3; i++) 
          {
              let children = []
              
              for (let j = 0; j < 3; j++) {
                  if(prod[k])
                  {
                      children.push(this.productComponent(prod[k]));
                      k++;
                  }
              }
              table.push(<div className="row" key={ i }> { children } </div>)
          }
          return table
  
      }
      productComponent(child){
  
      
          return (<div className="col-md-4 product" key={ child._id }>
          <figure className="card card-product">
              <div className="img-wrap"><img src={ child.image } alt={child.title} /></div>
              <figcaption className="info-wrap">
                      <h4 className="title">{ child.title }</h4>
                      <p className="desc">{ child.desc }</p> 
              </figcaption>
              <div className="bottom-wrap">
                <div className="price-wrap h5 clearfix">
                    <span className="price-new">${ child.price }</span> 
                </div> 
            </div>
            
              <button onClick={()=>{this.onClicked(child._id)}} className="btn btn-info m-2">See Details</button> 
          </figure>
      </div>);
      }
  
      render() {
          const { allProducts } = this.props.product;
  
          return (
              <div className="container mt-5">
               
                  { this.showProducts(allProducts) }
                  
                      <PaginationComponent
                          totalItems={this.props.product.numOfProd}
                          pageSize={3}
                          onSelect={this.handleSelected}
                          />                
              </div>
              
  
  
          );
      
      }

  
}
const mapStateToProps = state => ({
  product: state.product
});

export default connect(mapStateToProps, { getDropdownProducts, addActivePage })(withRouter(ProductsFromDropdown));