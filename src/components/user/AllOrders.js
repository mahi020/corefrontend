import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserOrder } from '../../actions/authActions';
import UserSidebar from './UserSidebar';

class AllOrders extends Component {
  
  constructor(){
    super();
    this.showProducts = this.showProducts.bind(this);
  }

  showProducts(prod){

    let table = []

    for (let i = 0; i < prod.length; i++) 
    {
        let children = [];
        let ul1 = [];
        let li1 = [];
        let ul2 = [];
        let li2 = [];
        let semiFinal = [];
        let final = [];

        //console.log("Element No "+i);
        for(let j=0;j<prod[i].orderList.length;j++)
        {
         // console.log(prod[i].orderList[j].productId.title);
          li1.push(this.pushLi(prod[i].orderList[j].productId.title));
        }
        ul1.push(<ul key={ prod[i]._id +''+ prod[i].title}>{ li1 }</ul>);
        semiFinal.push(ul1);
        for(let j=0;j<prod[i].qty.length;j++)
        {
          //console.log(prod[i].qty[j]);
          li2.push(this.pushLi(prod[i].qty[j]));
        }
        ul2.push(<ul key={prod[i]._id +''+ prod[i].time}>{ li2 }</ul>);
        semiFinal.push(ul2);
        //console.log(prod[i].time);
        
        semiFinal.push(prod[i].total);
        semiFinal.push(prod[i].time);
        let status;
        if(prod[i].pending==true)
        {
            status="Pending";
        }
        else if(prod[i].delivered==true)
        {
          status = "Delivered";
        }
        semiFinal.push(status);
        //console.log(prod[i].total);
        final.push(this.productComponent(semiFinal));
        table.push(<tr key={ prod[i]._id }>{ final }</tr>);
        
    }
    return table

}
pushLi(obj)
{
  let rand=Math.random().toString(36).substring(2);
  return(
      <li key={ rand }>
        {obj}
      </li>
  );
}
productComponent(child){

    return (
        child.map(doc=>{
          let rand=Math.random().toString(36).substring(3);
          return (<td key = { rand }> {doc} </td>);
        })
    );
  }

    componentDidMount(){

      this.props.getUserOrder(this.props.auth.user.id);
    
    }

    render() {
      
      const { order } = this.props.auth;
      
     
      return (
          
        <div className="container-fluid mt-2 p-0" id="main">
        <br/> <br/>
        <UserSidebar/>
          <div className="row mediaMargin">          
            <div className="col-sm-12 mt-5">
            { order.length>0 ? (<div className="table-responsive">
                                  <h3> All Orders </h3> 
                        <table className="table table-striped">
                            <thead className="thead-inverse">
                                <tr>
                          
                                    <th>Order List</th>
                                    <th>Quantities</th>
                                    <th>Total Amount</th>
                                    <th>Time</th>
                                    <th>Order Status</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                { this.showProducts(order) }
                            </tbody>
                        </table>
                    </div>): (<h3>No Orders Found!!</h3>)}
                    
            </div>
          
          </div>
            
        
            <hr/>


        </div>
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  auth: state.auth

});

export default connect(mapStateToProps, { getUserOrder })(AllOrders);
  