import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserProducts } from '../../actions/authActions';
import { withRouter } from 'react-router-dom';
import UserSidebar from './UserSidebar';

class AllProducts extends Component {


  constructor(){
    super();
    this.showProducts = this.showProducts.bind(this);
    this.onClickTr = this.onClickTr.bind(this);
}

    componentDidMount(){

      this.props.getUserProducts(this.props.auth.user.id);
      

    }
    onClickTr(id){

      this.props.history.push(`/showUserProduct/${id}`);

    }
    showProducts(prod){

      let table = []
      for (let i = 0; i < prod.length; i++) 
      {
          let children = []
          children.push(this.productComponent(prod[i]));             
          table.push(children)
      }
      return table

  }
  productComponent(child){

    
      return (
          <tr key={child._id} onClick = {()=>{ this.onClickTr(child._id) }}>
              <td> { child.title } </td>
              <td> { child.desc } </td>
              <td> { child.category } </td>
              <td> ${ child.price } </td>
              <td> { child.quantity } </td>
              <td> { child.approved? "Already Approved": "Not Approved" } </td>
          </tr>
      );
  }


    render() {
      const { userProducts } = this.props.auth;
      //console.log(this.props.private.allProducts);
      return (
          
        <div className="container-fluid mt-2 p-0" id="main">
          <br/> <br/>
          <UserSidebar/>
          <div className="row mediaMargin">
           <div className="col-sm-12 mt-5">
              { userProducts.length>0? (<div><h3>Your All Products</h3>
                    <div className="table-responsive">
                        <table className="table table-striped">
                            <thead className="thead-inverse">
                                <tr>
                                    
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Approved</th>
                                  
                                    
                                </tr>
                            </thead>
                            <tbody>
                                { this.showProducts(userProducts) }
                            </tbody>
                        </table>
              </div> </div> ): <h3>No Products</h3>}
            
            </div>
          
          </div>
            
        
            <hr/>


        </div>
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  auth: state.auth

});

export default connect(mapStateToProps, { getUserProducts })(withRouter(AllProducts));
  