import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { addProduct } from '../../actions/productActions';
import { withRouter } from 'react-router-dom';
//import objectToFormdata from 'object-to-formdata';
import FormData from 'form-data';
class AddProduct extends Component {

    
    constructor() {
        super();
        this.state = {
          title: '',
          desc: '',
          price: '',
          category:'',
          quantity:'',
          //image: null,
          errors: {},
          items: ['Books','Clothing','Electronics','Instrument', 'Other']
        };
        this.formData = new FormData();
        this.image = null;
        this.category='';
        this.handleChange = this.handleChange.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeDropdown = this.onChangeDropdown.bind(this);
      }
      componentWillReceiveProps(nextProps)
      {
        if(nextProps.errors)
        {
          console.log(nextProps.errors);
          this.setState({ errors: nextProps.errors });  
        }
        
      }
      onChange(e) {
          this.setState({ [e.target.name]: e.target.value });
      }
      handleChange(selectorFile)
      {
          
          
          this.image = selectorFile[0]; 
          //console.log(this.image);
          /*const files = Array.from(e.target.files);

          files.forEach((file, i) => {
              //console.log(i+" "+file) ;
              this.formData.append(i, file)
          });
          */
          
      }
      onChangeDropdown(e){
    
        
        //this.props.addAProductToCart(this.props.product.cart, prod, e.target.value);
        //console.log(prod, e.target.value);
        this.category = e.target.value;
        
      }

      onSubmit(e) {
        e.preventDefault();
        if(this.category.length==0)
        {
          //console.log('lol');
          this.category = 'Books';
        } 
        this.formData.set('title', this.state.title);
        this.formData.append('image', this.image);
        //this.formData.set('image',this.image);
        this.formData.set('desc', this.state.desc);
        this.formData.set('price', this.state.price);
        this.formData.set('category', this.category);
        this.formData.set('quantity', this.state.quantity);
        this.formData.set('author', this.props.auth.user.id);
        
        //console.log(this.state.category);
        let newProduct = {

          title: this.state.title,
          image: this.image.name,
          desc: this.state.desc,
          price: this.state.price,
          category: this.category,
          quantity: this.state.quantity,
          author: this.props.auth.user.id
        };

        //console.log(newProduct);
        //console.log(productData);
        //const newProduct = objectToFormdata(productData);
        //console.log("React "+this.formData);
        //console.log(this.image.name);
        this.props.addProduct(this.formData, newProduct ,this.props.history);
        
      }
    
      
  render() {

      const { errors } = this.state;
      return (
        <div className="m-5">
        <br/> <br/> <br/>
        <div className="container shadow" id="addProduct">
          <div className="row">

            <div className="col-md-4">
              <div id="bekaar"> </div>
            </div>
            <div className="col-md-8">
              <form encType="multipart/form-data" onSubmit={this.onSubmit}>
                  <h3>Add Your Product</h3>
                  <br/>
                  <div className="form-group">
                  <label>Title</label>
                  <input type="text"
                    className={classnames('form-control', {
                            'is-invalid': errors.title
                    })} 
                    id="title" placeholder="Add a Title" name="title" required value={this.state.title}
                    onChange={this.onChange}/>
                    {errors.title && (
                      <div className="invalid-feedback">{errors.title}</div>
                    )}
                  </div>
              
                <div className="form-group">
                  <label>Add an Image File</label>
                  <input type="file"
                  className={classnames('form-control-file', {
                    'is-invalid': errors.image
                    })}
                  name="image" id="image" accept="image/*" required onChange={ (e) => this.handleChange(e.target.files) }/>
                  {errors.image && (
                      <div className="invalid-feedback">{errors.image}</div>
                    )}
                </div>

                <div className="form-group">
                <label>Description</label>
                <textarea className={classnames('form-control', {
                            'is-invalid': errors.desc
                    })} name="desc" id="desc" rows="3" required value={this.state.desc}
                  onChange={this.onChange}></textarea>
                  {errors.desc && (
                      <div className="invalid-feedback">{errors.desc}</div>
                  )}
                </div>

                <div className="form-group">
                <label>Price</label>
                <input type="text" className={classnames('form-control', {
                            'is-invalid': errors.price
                    })} id="price" name="price" required value={this.state.price} onChange={this.onChange}/>
                  {errors.price && (
                      <div className="invalid-feedback">{errors.price}</div>
                  )}
                </div>

              <div className="form-group">
                <label>Category</label>
                    <select className="form-control" defaultValue="0" 
                    onChange={(e)=>{this.onChangeDropdown(e)}}>
                      {this.state.items.map((item, index)=>{

                        return(<option key={index}>{ item }</option>);

                      })}
                    </select>
              </div>
              <div className="form-group">
                <label>Quantity</label>
                <input type="number" 
                  className={classnames('form-control', {
                    'is-invalid': errors.quantity
                    })}           
                  id="quantity" name="quantity" required value={this.state.quantity} onChange={this.onChange}/>
                  {errors.quantity && (
                      <div className="invalid-feedback">{errors.quantity}</div>
                  )}
              </div>
              <button type="submit" className="btn btn-info">Submit</button>
              </form>
              </div>
              </div>
              </div>
    </div>
    );
  }
}

const mapStateToProps = state => ({
    auth: state.auth,
    product: state.product,
    errors: state.errors
  });
export default connect(mapStateToProps , { addProduct })(withRouter(AddProduct));
