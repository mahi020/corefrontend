import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class UserSidebar extends Component {

    
    render(){
        const styles = {
            color:'#FFFFFF'
         };
        return (
            <div className="sidebar pl-0">
             
                <ul className="nav flex-column text-center" id="border">
                <br/> <br/>
                <li className="nav-item">
                      <Link className="nav-link" to="/me" style={styles}>Home</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/userOrder" style={styles}>All Orders</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/addProduct" style={styles}>Add Product</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/myProducts" style={styles}>My Products</Link>
                </li>

                </ul>
            </div>

        );
    }

}

export default UserSidebar;
