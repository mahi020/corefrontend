import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateProduct, getAProduct } from '../../actions/productActions';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
//import objectToFormdata from 'object-to-formdata';
//import FormData from 'form-data';
class UpdateProduct extends Component {

    
    constructor() {
        super();
        this.state = {

          id:'',
          price: '',
          quantity:'',
          //image: null,
          errors: {}

        };
        //this.formData = new FormData();
        //this.image = null;
        //this.handleChange = this.handleChange.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
      }
      componentWillReceiveProps(nextProps)
      {
          
          this.setState({

            price: nextProps.product.price,
            quantity: nextProps.product.quantity

          });
        if(nextProps.errors)
        {
          console.log(nextProps.errors);
          this.setState({ errors: nextProps.errors });  
        }
      }

      componentDidMount(){

            this.props.getAProduct(this.props.match.params.id);
            this.setState({id: this.props.match.params.id});
            //console.log(this.state.price);
            //console.log(this.state.quantity);


      }
      onChange(e) {
          this.setState({ [e.target.name]: e.target.value });
      }
      /*handleChange(selectorFile)
      {
          
          
          this.image = selectorFile[0]; 
          //console.log(this.image);
          //const files = Array.from(e.target.files);

          //files.forEach((file, i) => {
              //console.log(i+" "+file) ;
            //  this.formData.append(i, file)
          //});
          
          
      }
      */
      

      onSubmit(e) {
        e.preventDefault();
        
        
        const productData = {
        
          price: this.state.price,
          quantity: this.state.quantity

        };
        
          
        /*
        this.formData.append('image', this.image);        
        this.formData.set('price', this.state.price);
        this.formData.set('quantity', this.state.quantity);
        */

        //console.log(productData);
        //const newProduct = objectToFormdata(productData);
        //console.log("React "+this.formData);
        this.props.updateProduct(productData, this.state.id ,this.props.history);
        
      }
    
      
  render() {
    const { errors } = this.state;
      return (
        <div className="m-5">
        <br/> <br/> <br/>
        <form onSubmit={this.onSubmit}>

          <div className="form-group">
           <label>Price</label>
           <input type="number" 
           className={classnames('form-control', {
            'is-invalid': errors.price
            })}
           id="price" name="price" required value={this.state.price} onChange={this.onChange}/>
           {errors.price && (
                <div className="invalid-feedback">{errors.price}</div>
            )}
          </div>

         <div className="form-group">
           <label>Quantity</label>
           <input type="number" 
           className={classnames('form-control', {
            'is-invalid': errors.quantity
            })} 
           id="quantity" name="quantity" required value={this.state.quantity} onChange={this.onChange}/>
           {errors.quantity && (
                <div className="invalid-feedback">{errors.quantity}</div>
            )}
         </div>
         <button type="submit" className="btn btn-info">Submit</button>
        </form>
        </div>
    );
  }
}

const mapStateToProps = state => ({
    auth: state.auth,
    product: state.product,
    errors: state.errors
  });
export default connect(mapStateToProps , { updateProduct, getAProduct })(withRouter(UpdateProduct));
