import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAUserProduct } from '../../actions/authActions';
import { deleteAProduct } from '../../actions/productActions';
import { withRouter } from 'react-router-dom';

class ShowAProduct extends Component {
  
  constructor(){
    super();
    this.state = {
      title:'',
      desc:'',
      category:'',
      price:'',
      quantity:'',
      approved:'',
      image:''
    };
    this.onClick = this.onClick.bind(this);
    this.onClickDelete = this.onClickDelete.bind(this);
  }
  componentDidMount()
  {
      this.props.getAUserProduct(this.props.match.params.id);
  }
  componentWillReceiveProps(nextProps)
  {
    this.setState({
      title: nextProps.auth.singleProduct.title,
      desc: nextProps.auth.singleProduct.desc,
      category: nextProps.auth.singleProduct.category,
      price: nextProps.auth.singleProduct.price,
      quantity: nextProps.auth.singleProduct.quantity,
      image: nextProps.auth.singleProduct.image
    });
  }
  onClick(id){

        this.props.history.push(`/updateProduct/${id}`);

  }
  onClickDelete(id)
  {
    this.props.deleteAProduct(id, this.props.history);
  }
  render() {
      
      const { singleProduct } = this.props.auth;


      return (
          
        <div className="colorlib-shop mt-5">
        <div className="container">
        <br/> <br/> <br/>
          <div className="row row-pb-lg">
            <div className="col-md-10 col-md-offset-1">
              <div className="product-detail-wrap">
                <div className="row">
                  <div className="col-md-5">
                    <div className="product-entry">
                      <div className="product-img">
                        <img src={ this.state.image }/>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-7">
                    <h3> Product Details </h3>
                    <br/> 
                    <div className="form-group">
                      <label>Title</label>
                      <input type="text" className="form-control" id="title" name="title" value={this.state.title} disabled="disabled"/>
                    </div>

                    <div className="form-group">
                      <label>Description</label>
                      <textarea className="form-control" name="desc" id="desc" rows="3" value={this.state.desc} disabled="disabled"></textarea>
                    </div>


                    <div className="form-group">
                      <label>Category</label>
                      <input type="text" className="form-control" id="category" name="category" value={this.state.category} disabled="disabled"/>
                    </div>

                    <div className="form-group">
                      <label>Price</label>
                      <input type="text" className="form-control" id="price" name="price" value={this.state.price} disabled="disabled"/>
                    </div>

                    <div className="form-group">
                      <label>Quantity</label>
                      <input type="text" className="form-control" id="quantity" name="quantity" value={this.state.quantity} disabled="disabled"/>
                    </div>

                    <div className="clearfix">
                      <button className="btn btn-info pull-right" onClick={()=>{ this.onClick(singleProduct._id) }}>Edit</button>

                      <button className="btn btn-danger pull-right mr-2" onClick={()=>{ this.onClickDelete(singleProduct._id) }}>Delete</button>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
  
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  auth: state.auth

});

export default connect(mapStateToProps, { getAUserProduct, deleteAProduct })(withRouter(ShowAProduct));
  