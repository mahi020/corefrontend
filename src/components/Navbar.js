import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../actions/authActions';
import { setSearchedProducts, setDropdownProducts } from '../actions/productActions';
import { withRouter } from 'react-router-dom';

class Navbar extends Component {

  constructor()
  {
    super();
    this.state = {

      search:'',

    };
  }

  onLogoutClick = (e) => {
    e.preventDefault();
    window.location.href = '/login';
    this.props.logoutUser();

  }
  onSubmit = (e) => {
    e.preventDefault();

    //console.log(this.state.search);
    this.props.setSearchedProducts(this.state.search, this.props.history);
    this.setState({
      search:''
    });
    
  }
  onChange = (e) => {
    this.setState({ search: e.target.value });
  }
  onDropdown = (category) =>{
    
    this.props.setDropdownProducts(category, this.props.history);
    
  };
  

  render() {
    const { isAuthenticated, isAdmin, isModerator } = this.props.auth;
    var userLink;
    const styles = {
       color:'#1e90ff'
    };
    if(isAuthenticated && !isAdmin && !isModerator)
    {
        userLink = (
          
          <li className="nav-item">
          <Link
            to="/me"
            className="nav-link"
            style={styles}
          >
          {this.props.auth.user.firstname}
          {' '}
          <i className="fa fa-user-circle"></i>
          </Link>
        </li>

        );    
    }
    else if(isAuthenticated && !isAdmin && isModerator)
    {
        userLink = (
          
          <li className="nav-item">
          <Link
            to="/moderator/me"
            className="nav-link"
            style={styles}
          >
          {this.props.auth.user.firstname}
          {' '}
          <i className="fa fa-user-circle"></i>
          </Link>
        </li>

        );    
    }
    else if(isAuthenticated && isAdmin && !isModerator)
    {
        userLink = (
          
          <li className="nav-item">
          <Link
            to="/admin/me"
            className="nav-link"
            style={styles}
          >
          
          
          {this.props.auth.user.firstname}
          {' '}
          <i className="fa fa-user-circle"></i>
          </Link>
        </li>

        );    
    }

    const authLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item dropdown">
            <Link className="nav-link dropdown-toggle" to="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={styles}>
              Categories
            </Link>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
            
              <Link className="dropdown-item" to="/dropdown/Books" onClick={()=>{ this.onDropdown("Books") }} style={styles}>Books</Link>
              <Link className="dropdown-item" to="/dropdown/Clothing" onClick={()=>{ this.onDropdown("Clothing") }} style={styles}>Clothing</Link>
              <Link className="dropdown-item" to="/dropdown/Electronics" onClick={()=>{ this.onDropdown("Electronics") }} style={styles}>Electronics</Link>
              <Link className="dropdown-item" to="/dropdown/Instrument" onClick={()=>{ this.onDropdown("Instrument") }} style={styles}>Instrument</Link>
              <Link className="dropdown-item" to="/dropdown/Other" onClick={()=>{ this.onDropdown("Other") }} style={styles}>Other</Link>
              
            </div>
            </li>
        <li className="nav-item">
            <Link className="nav-link" to="/cart" style={styles}><i className="fa fa-shopping-cart mr-1"></i> Cart <span className="badge badge-info ml-1">{this.props.product.cart.length>0? this.props.product.cart.length: ''}</span></Link>
        </li>
        { userLink }
        <li className="nav-item">
          <a
            href=""
            onClick={this.onLogoutClick.bind(this)}
            className="nav-link"
            style={styles}
          >
          {' '}
            Logout
          </a>
        </li>
      </ul>
    );

    const guestLinks = (
      <ul className="navbar-nav ml-auto">
      <li className="nav-item dropdown">
            <Link className="nav-link dropdown-toggle" to="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={styles}>
              Categories
            </Link>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link className="dropdown-item" to="/dropdown/Books" onClick={()=>{ this.onDropdown("Books") }} style={styles}>Books</Link>
              <Link className="dropdown-item" to="/dropdown/Clothing" onClick={()=>{ this.onDropdown("Clothing") }} style={styles}>Clothing</Link>
              <Link className="dropdown-item" to="/dropdown/Electronics" onClick={()=>{ this.onDropdown("Electronics") }} style={styles}>Electronics</Link>
              <Link className="dropdown-item" to="/dropdown/Instrument" onClick={()=>{ this.onDropdown("Instrument") }} style={styles}>Instrument</Link>
              <Link className="dropdown-item" to="/dropdown/Other" onClick={()=>{ this.onDropdown("Other") }} style={styles}>Other</Link>
            </div>
            </li>
        <li className="nav-item">
            <Link className="nav-link" to="/cart" style={styles}><i className="fa fa-shopping-cart"></i> <span>Cart</span> <span className="badge badge-info ml-1">{this.props.product.cart.length>0? this.props.product.cart.length: ''}</span></Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/register" style={styles}>
            Sign Up
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/login" style={styles}>
            Login
          </Link>
        </li>
      </ul>
    );

    return (
      <nav className="navbar navbar-expand-lg navbar-light shadow-sm fixed-top" style={{backgroundColor:'#FFFFFF'}}>
          <Link to="/" className="navbar-brand ml-3">E-<span style={styles}>Shop</span></Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

        <div className="collapse navbar-collapse"> </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <form className="form-inline my-2 my-lg-0" onSubmit={this.onSubmit} >
              <input className="form-control mr-sm-2" type="search" placeholder="Search here ..." aria-label="Search" name="search" value={this.state.search}
              onChange={this.onChange} style={{ borderRadius:'2rem' }}/>
              <button className="btn btn-outline-info my-2 my-sm-0" type="submit" style={{ borderRadius:'2rem' }}>Search</button>
            </form>
          </ul>
         {isAuthenticated ? authLinks : guestLinks}
        
      </div>
    </nav>

    );
  }
}
const mapStateToProps = state => ({
  auth: state.auth,
  product: state.product
});

export default connect(mapStateToProps, { logoutUser, setSearchedProducts, setDropdownProducts })(withRouter(Navbar));