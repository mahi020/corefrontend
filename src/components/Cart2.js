import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { removeItemFromCart, saveOrder, setTotal, setIncrementedTotal } from '../actions/productActions';
import { addAProductToCart, removeItemFromCart, sendToLogin } from '../actions/productActions';
import { getShipping, setShippingError} from '../actions/authActions';
import Checkout from './Checkout';
import { withRouter, Link } from 'react-router-dom';
import classnames from 'classnames';

class Cart2 extends Component {
    constructor(){
      super();
      this.state = {
        shipping:{},
        checkoutButton: false,
        errors: {},
        address1: '',
        address2:'',
        country1:'',
        country2:'',
        country: ['Australia','Bangladesh','Canada', 'Denmark','England', 'Finland','Germany','Hungary','Italy', 'India', 'Norway','Paraguay', 'Russia', 'Sweden', 'USA'],
        zip1:'',
        zip2:'',
        checked: false        
      };
      this.items = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];
      this.onChange = this.onChange.bind(this);
      this.onRemove = this.onRemove.bind(this);
      this.onClick = this.onClick.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
      this.onChangeDropdown = this.onChangeDropdown.bind(this);
      this.handleOptionChange = this.handleOptionChange.bind(this); 
      this.onChangeShipping = this.onChangeShipping.bind(this);
      this.total = 0;
      this.selected = null;
      this.country ='';
    }

    onClick()
    {
      //console.log(this.props.product.cart);
      //console.log(this.props.auth.user.id);
      //console.log("Clicccked... ");
      this.props.sendToLogin(this.props.history);
    
      
    }
    componentDidMount() {
      // Note for Shipping Address:
      // If User is Authenticated then check if user has Shipping address
      // if has then show it in radio button and form where user can give another shipping address
      // if not then show the form only where user can give the shipping address.
      if (this.props.auth.isAuthenticated) {

        this.props.getShipping(this.props.auth.user.id);
        
      }
    }
    componentWillReceiveProps(nextProps)
    {
      //console.log(nextProps);
      if(nextProps.errors)
      {
        this.setState({ errors: nextProps.errors });
      }

      if(nextProps.auth.address!=undefined && nextProps.auth.address.length>0)
      {
        this.setState({ address1: nextProps.auth.address});
      }
      
      if(nextProps.auth.country!=undefined && nextProps.auth.country.length>0)
      {
        this.setState({ country1: nextProps.auth.country });
      }

      if(nextProps.auth.zip!=undefined && nextProps.auth.zip.length>0)
      {
        this.setState({ zip1: nextProps.auth.zip });
      }

    }

    onSubmit(e){
      e.preventDefault();
      //console.log(e.target.value);
      let newObj = {};
      if(this.state.checked==true)
      {
        newObj = {
          address: this.state.address2,
          country: this.country,
          zip: this.state.zip2
        };

        this.props.setShippingError(newObj);
        console.log(this.state.address2);
        console.log(this.country);
        console.log(this.state.zip2);
        localStorage.setItem('address', this.state.address2);
        localStorage.setItem('country', this.country);
        localStorage.setItem('zip',this.state.zip2);
        this.setState({ checkoutButton: true });
      }
      else
      {
        console.log(this.state.address1);
        console.log(this.state.country1);
        console.log(this.state.zip1);
        localStorage.setItem('address', this.state.address1);
        localStorage.setItem('country', this.state.country1);
        localStorage.setItem('zip',this.state.zip1);
        this.setState({ checkoutButton: true });
      }
      
    }
    handleOptionChange(e)
    {
      console.log("Logged");
      this.setState({checked: !this.state.checked});
    }
    onChangeShipping(e)
    {
      this.setState({ [e.target.name]: e.target.value });
    }

    onChangeDropdown(e){

      console.log(e.target.value);
      this.country = e.target.value;
      //this.setState({ shippingAddress: e.target.value });

    }
    
    
    onChange(prod, e){
      
      
        //this.props.addAProductToCart(this.props.product.cart, prod)
        //console.log(prod, parseInt(e.target.value));
        this.props.addAProductToCart(prod, e.target.value);
        //console.log(this.props.product.cart);
      

    }
    
    onRemove(prod)
    {
        this.props.removeItemFromCart(prod);
    }

    render() {
      const { total } = this.props.product;
      const { errors } = this.state;
      //console.log(this.props.auth.shipping);
        const stripeButton = (
        <p className="App-intro btn pull-right clearfix">
          <Checkout
            name={'Pay With Stripe'}
            description={'Shop with E-Shop'}
            amount={total}
          />
        </p>
                 
      );

      const normalButton = (

        <div className="clearfix">
        <Link to="/login" className="pull-right" style={{marginRight: '120px'}}> Login Before Checkout </Link>
        </div>

      );
      
      const myCart = (
        <div className="container">
                    <br/> <br/> <br/> <br/> 
                    <h1 className="text-center">My Cart</h1>
                    <hr/>
                  <div className="table-responsive-sm">
                    <table className="table table-hover shopping-cart-wrap">
                     <thead className="text-muted">
                      <tr>
                       <th scope="col">Product</th>
                       <th scope="col" width="120">Quantity</th>
                       <th scope="col" width="120">Price</th>
                       <th scope="col" width="200" className="text-right">Action</th>
                      </tr>
                     </thead>
                     <tbody>
                      {this.props.product.cart.map(prod=>{
                       return (<tr key={prod._id}>
                       <td>
                        <figure className="media">
                         <div className="img-wrap mr-2"><img src={ prod.image } className="img-thumbnail img-sm"/></div>
                         <figcaption className="media-body">
                          <h6 className="title text-truncate">{ prod.title }</h6>
                         </figcaption>
                        </figure> 
	                   </td>  
                       <td>
                        <select className="form-control" defaultValue={prod.qty} onChange={(e)=>{this.onChange(prod,e)}}>
                        {this.items.slice(0, prod.quantity).map((item, index)=>{

                            return(<option key={index +''+prod._id}>{ item }</option>);

                        })}
                        </select>
                       </td>
                       <td>
                        <div className="price-wrap">
                         <var className="price">${ prod.price }</var>
                        </div> 
	                   </td>
                       <td className="text-right">
                        <button className="btn btn-outline-danger" onClick={()=>{
                          this.onRemove(prod)
                        }}> × Remove</button>
                       </td>
                    </tr>);
                    })}
                    <tr key="abcedfgh">
                    <td>
                      <input type="hidden" value="Nothing is There"/>
                    </td>
                    <td>
                      <input type="hidden" value="Nothing is There"/>
                    </td>

                      <td>
                      <p>Total = ${ total }</p>
                      </td>
                      
                    </tr>
                   </tbody>
                  </table>
                 </div>
                  
                </div>
      );
      const noCart = (
        <div>
          <br></br>
          <br></br>
          <br></br>
        <h3 className="text-center m-5">There are no items in this cart</h3>
        </div>
      
      );

      const shippingForm2 = (

          <div className="container d-flex justify-content-center">
            
            <form onSubmit={this.onSubmit}>
            <h4 className="mb-3"> Give Your Shipping Address Below: </h4>
            <div className="form-group">
                  <input
                    type="text"
                    className={classnames('form-control form-control-lg', {
                      'is-invalid': errors.address
                    })}
                    placeholder="Address"
                    name="address2"
                    value={this.state.address2}
                    onChange={this.onChangeShipping}
                    required
                  />
                  {errors.address && (
                    <div className="invalid-feedback">{errors.address}</div>
                  )}
                </div>
                <div className="row">
                <div className="col-md-5 mb-3">
                  <label>Country</label>
                    <select className="custom-select d-block w-100" defaultValue="0" name="country2" required onChange={this.onChangeDropdown}>
                      {
                        this.state.country.map((item,index)=>{

                          return (<option key={index}> { item } </option>);

                        })
                      }
                    </select>
                </div>

                <div className="col-md-4 mb-3">
                  <label>Zip Code</label>
                  <input type="text" 
                  className={classnames('form-control', {
                      'is-invalid': errors.zip
                    })} 
                    name="zip2" required
                    value={this.state.zip2}
                    onChange={this.onChangeShipping}
                    />
                  {errors.zip && (
                    <div className="invalid-feedback">{errors.zip}</div>
                  )}
                </div>

            </div>

            { this.props.auth.isAuthenticated && this.state.checkoutButton? '': (<div     className="checkbox">
              <label>
                <input type="checkbox" name="option1" value="option1" 
                              checked={this.state.checked} 
                              onChange={this.handleOptionChange} />
                {' '}Add New Address
              </label>
            </div>
            )}
            
            { this.props.auth.isAuthenticated && this.state.checkoutButton ? stripeButton: (<div className="clearfix">
              <button className="btn btn-danger mt-3 mb-3 pull-right">Next</button>
            </div>)}
            
            </form>
          </div>
      );
      
      const shippingForm1 = (

        <div className="container d-flex justify-content-center">
            
            <form onSubmit={this.onSubmit}>
            <h4 className="mb-3"> Give Your Shipping Address Below: </h4>
            <div className="form-group">
                  <input
                    type="text"
                    className={classnames('form-control form-control-lg', {
                      'is-invalid': errors.address
                    })}
                    placeholder="Address"
                    name="address1"
                    value={this.state.address1}
                    disabled="disabled"
                  />
                  {errors.address && (
                    <div className="invalid-feedback">{errors.address}</div>
                  )}
                </div>
                <div className="row">
                <div className="col-md-5 mb-3">
                  <label>Country</label>
                  <input type="text" 
                  className={classnames('form-control', {
                      'is-invalid': errors.country1
                    })} 
                    name="zip1"
                    value={this.state.country1}
                    disabled="disabled"
                    />
                </div>

                <div className="col-md-4 mb-3">
                  <label>Zip Code</label>
                  <input type="text" 
                  className={classnames('form-control', {
                      'is-invalid': errors.zip
                    })} 
                    name="zip1"
                    value={this.state.zip1}
                    disabled="disabled"
                    />
                  {errors.zip && (
                    <div className="invalid-feedback">{errors.zip}</div>
                  )}
                </div>

            </div>
            
            { this.props.auth.isAuthenticated && this.state.checkoutButton? '': (<div     className="checkbox">
              <label>
                <input type="checkbox" name="option1" value="option1" 
                              checked={this.state.checked} 
                              onChange={this.handleOptionChange} />
                {' '}Add New Address
              </label>
            </div>
            )}
            
            { this.props.auth.isAuthenticated && this.state.checkoutButton ? stripeButton: (<div className="clearfix">
              <button className="btn btn-danger mt-3 mb-3 pull-right">Next</button>
            </div>)}
            
            </form>
          </div>   
        
      );
        return (
            <div>

              {this.props.product.cart.length>0? myCart : noCart}
                  
              { this.props.auth.isAuthenticated && this.props.product.cart.length>0 &&this.state.checked? shippingForm2:this.props.auth.isAuthenticated && this.props.product.cart.length>0 && !this.state.checked? shippingForm1:''}
              
              { this.props.auth.isAuthenticated===false && this.props.product.cart.length>0? normalButton:'' }
            </div>
                );
                              
        }
}
                            
const mapStateToProps = state =>({

  product: state.product,
  auth: state.auth,
  errors: state.errors

});

export default connect(mapStateToProps, { addAProductToCart, removeItemFromCart, sendToLogin, getShipping, setShippingError })(withRouter(Cart2)) ;