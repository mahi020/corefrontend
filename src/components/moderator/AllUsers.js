import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllUsers } from '../../actions/privateActions';
import ModeratorSidebar from './ModeratorSidebar';

class AllUsers extends Component {

    constructor(){
        super();
        this.showProducts = this.showProducts.bind(this);
    }
    componentDidMount(){

      this.props.getAllUsers();
      
    }
    
    showProducts(prod){

        let table = []
        for (let i = 0; i < prod.length; i++) 
        {
            let children = []
            if(this.props.auth.user.id !== prod[i]._id)
            {
              children.push(this.productComponent(prod[i]));             
              table.push(children);
            }
        }
        return table

    }
    productComponent(child){

    
        return (
            <tr key={child._id}>
                <td> { child.firstname } </td>
                <td> { child.lastname } </td>
                <td> { child.email } </td>
            </tr>
        );
    }


    render() {

      const { allUsers } = this.props.private;
      return (
          
        <div className="container-fluid mt-2 p-0" id="main">
        <br/> <br/>
          <ModeratorSidebar/>
          <div className="row mediaMargin">              
            <div className="col-sm-12 mt-5">
                    <div className="table-responsive">
                        <table className="table table-striped">
                            <thead className="thead-inverse">
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>E-mail</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                               { this.showProducts(allUsers) }
                            </tbody>
                        </table>
                    </div>
                </div>
          
          </div>
            
        
            <hr/>


        </div>
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  private: state.private,
  auth: state.auth

});

export default connect(mapStateToProps, { getAllUsers })(AllUsers);
  