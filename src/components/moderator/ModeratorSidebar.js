import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ModeratorSidebar extends Component {

    
    render(){
      const styles = {
        color:'#FFFFFF'
     };
        return (
            <div className="sidebar pl-0"> 
                <ul className="nav flex-column text-center" id="border">
                <br/> <br/>
                <li className="nav-item">
                    <Link className="nav-link" to="/moderator/me" style={styles}>Home</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/home" style={styles}>All Orders</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/allUsers" style={styles}>All Users</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/allProducts" style={styles}>All Products</Link>
                  </li>
                  
                </ul>
            </div>


        );
    }

}

export default ModeratorSidebar;
