import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllOrders, togglePendingStatus, toggleDeliveryStatus, toggleReceivedStatus } from '../../actions/privateActions';
import ModeratorSidebar from './ModeratorSidebar';
import { withRouter } from 'react-router-dom';

class AllOrders extends Component {
  
  constructor(){
    super();
    this.showProducts = this.showProducts.bind(this);
    this.pendingStatus = this.pendingStatus.bind(this);
    this.deliveryStatus = this.deliveryStatus.bind(this);
    this.receivedStatus = this.receivedStatus.bind(this);

  }

  showProducts(prod){

    let table = []

    for (let i = 0; i < prod.length; i++) 
    {
        let children = [];
        let ul1 = [];
        let li1 = [];
        let ul2 = [];
        let li2 = [];
        let semiFinal = [];
        let final = [];

        //console.log("Element No "+i);
        //console.log(prod[i].userId.firstname);
        semiFinal.push(prod[i].userId.firstname);
        for(let j=0;j<prod[i].orderList.length;j++)
        {
         // console.log(prod[i].orderList[j].productId.title);
          li1.push(this.pushLi(prod[i].orderList[j].productId.title));
        }
        ul1.push(<ul key={ prod[i]._id +''+ prod[i].title}>{ li1 }</ul>);
        semiFinal.push(ul1);
        for(let j=0;j<prod[i].qty.length;j++)
        {
          //console.log(prod[i].qty[j]);
          li2.push(this.pushLi(prod[i].qty[j]));
        }
        ul2.push(<ul key={prod[i]._id +''+ prod[i].time}>{ li2 }</ul>);
        semiFinal.push(ul2);
        //console.log(prod[i].time);
        
        semiFinal.push(prod[i].total);
        semiFinal.push(prod[i].time);

        let pending = {
          pending: prod[i].pending,
          id: prod[i]._id
        };
        semiFinal.push(pending);
        let delivered = {
          delivered: prod[i].delivered,
          id: prod[i]._id
        };
        semiFinal.push(delivered);
        //console.log(prod[i].total);
        final.push(this.productComponent(semiFinal));
        table.push(<tr key={ prod[i]._id } >{ final }</tr>);
        
    }
    return table

}

pendingStatus(data){

  //console.log(data);
  this.props.togglePendingStatus(data, this.props.history);
  

}

deliveryStatus(data){
  
  //console.log(data);
  this.props.toggleDeliveryStatus(data, this.props.history);
}

receivedStatus(data){
  //console.log(data);
  this.props.toggleReceivedStatus(data, this.props.history);
}

pushLi(obj)
{
  let rand=Math.random().toString(36).substring(2);
  return(
      <li key={ rand }>
        {obj}
      </li>
  );
}
productComponent(child){

    return (
        child.map(doc=>{
          let rand=Math.random().toString(36).substring(3);
          console.log(doc);

          if(typeof(doc)==='object' && 'pending' in doc)
          {
            let successButton = (<i class="fa fa-check ml-4" aria-hidden="true" style={{color:'green'}}></i>);
            let dangerButton = (<button className="btn btn-info ml-4" onClick={()=>{this.pendingStatus(doc)}}></button>);
            return (<td key = { rand }> { doc.pending ? successButton: dangerButton } </td>);  
          }
          else if(typeof(doc)==='object' && 'delivered' in doc)
          {
            let successButton = (<i class="fa fa-check ml-4" aria-hidden="true" style={{color:'green'}}></i>);
            let dangerButton = (<button className="btn btn-info ml-4" onClick={()=>{this.deliveryStatus(doc)}}></button>);
            
            return (<td key = { rand }> { doc.delivered ? successButton: dangerButton } </td>);  
          }
          return (<td key = { rand }> {doc} </td>);
        })
    );
  }

    componentDidMount(){

      this.props.getAllOrders();
    
    }

    render() {
      
      const { allOrders } = this.props.private;
      return (
          
        <div className="container-fluid mt-2 p-0" id="main">
        <br/> <br/>
          <ModeratorSidebar/>
          <div className="row mediaMargin">
            <div className="col-sm-12 mt-5">
                    <div className="table-responsive">
                        <table className="table table-striped">
                            <thead className="thead-inverse">
                                <tr>
                                    <th>Name</th>
                                    <th>Order List</th>
                                    <th>Quantities</th>
                                    <th>Total Amount</th>
                                    <th>Time</th>
                                    <th>Pending</th>
                                    <th>Delivered</th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.showProducts(allOrders) }
                            </tbody>
                        </table>
                    </div>
                </div>
          
          </div>
            
        
            <hr/>


        </div>
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  private: state.private

});

export default connect(mapStateToProps, { getAllOrders, togglePendingStatus, toggleDeliveryStatus, toggleReceivedStatus })(withRouter(AllOrders));
  