import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllProducts } from '../../actions/privateActions';
import { withRouter } from 'react-router-dom';
import ModeratorSidebar from './ModeratorSidebar';

class AllProducts extends Component {


  constructor(){
    super();
    this.showProducts = this.showProducts.bind(this);
    this.onClickTr = this.onClickTr.bind(this);
    }

    componentDidMount(){

      this.props.getAllProducts();
      

    }
    onClickTr(id){

      this.props.history.push(`/showAProduct/${id}`);

    }
    showProducts(prod){

      let table = []
      for (let i = 0; i < prod.length; i++) 
      {
          let children = []
          children.push(this.productComponent(prod[i]));             
          table.push(children)
      }
      return table

  }
  productComponent(child){

    
      return (
          <tr key={child._id} onClick = {()=>{ this.onClickTr(child._id) }}>
              <td> { child.title } </td>
              <td> { child.desc } </td>
              <td> { child.category } </td>
              <td> $ { child.price } </td>
              <td> { child.quantity } </td>
              <td> { child.approved? "Already Approved": "Not Approved" } </td>
          </tr>
      );
  }


    render() {
      const { allProducts } = this.props.private;
      //console.log(this.props.private.allProducts);
      return (
          
        <div className="container-fluid mt-2 p-0" id="main">
        <br/> <br/>
        <ModeratorSidebar/>
          <div className="row mediaMargin">
            <div className="col-sm-12 mt-5">
            <h3>All Products Added By Users</h3>
                    <div className="table-responsive">
                        <table className="table table-striped">
                            <thead className="thead-inverse">
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Approved</th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.showProducts(allProducts) }
                            </tbody>
                        </table>
                    </div>
                </div>
          
          </div>
            
        
            <hr/>


        </div>
      
      );
    }
  
    
}

const mapStateToProps = state =>({

  private: state.private

});

export default connect(mapStateToProps, { getAllProducts })(withRouter(AllProducts));
  