import React, { Component } from 'react';
import { connect } from 'react-redux';
import StarRatings from 'react-star-ratings';
import Modal from 'react-responsive-modal';
import { withRouter } from 'react-router-dom';

import { setReview, sendUpdatedReview, setAfterRemovingReview } from '../actions/productActions';


class AllComments extends Component{

    constructor(){
        super();
        this.state = {
            editClicked: false,
            open:false,
            comment:'',
            rating:0
        }
        this.onClickDelete = this.onClickDelete.bind(this); 
        this.changeRating = this.changeRating.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onDeleteByAuthority = this.onDeleteByAuthority.bind(this);
           
    }
    onOpenModal = () => {

        this.props.setReview(this.props.auth.user.id,this.props.product.item._id);
        this.setState({ open: true });
        /*this.setState({
            comment: this.props.product.message,
            rating: this.props.product.rating
        });
        */
        
       
        
    };
    
    onCloseModal = () =>{

        
          this.setState({ open: false });
        
    };

    handleChange(event) {
        this.setState({comment: event.target.value});
        //console.log(this.state.comment);
    }
    changeRating(newRating) {

        //console.log(newRating);
            //let rate = this.props.product.item.rating;
            //console.log(this.props.product.item);
            //console.log(rate);
            this.setState({rating: newRating});
        
    
    }
    componentWillReceiveProps(nextProps)
    {
        this.setState({comment: nextProps.product.message,
            rating:nextProps.product.rating    
        });
    }
    onClickDelete(){

        this.props.setAfterRemovingReview(this.props.auth.user.id, this.props.product.item._id, this.props.history);       
    }

    onClick(){


        this.props.sendUpdatedReview(this.state.rating, this.state.comment, this.props.auth.user.id, this.props.product.item._id, this.props.history);
        

    }
    onDeleteByAuthority(id)
    {
        this.props.setAfterRemovingReview(id, this.props.product.item._id, this.props.history);
    }

    render()
    {
        let comments = this.props.product.comments;
        let updateButton = (<div className="btn-group btn-group-sm">
                <button className="btn btn-info" onClick={this.onOpenModal}>
                    Edit
                </button>{' '}
                <button type="button" className="btn btn-danger ml-2" onClick={this.onClickDelete}>Delete</button>
                    <Modal open={this.state.open} onClose={this.onCloseModal} focusTrapped>
                    <h3>Edit Your Review</h3>
                    <div className="m-5">
                <div>
                <div className="mb-4">
                    <StarRatings
                    rating={this.state.rating}
                    starRatedColor= 'rgba(255,195,0,0.8)'
                    changeRating={this.changeRating}
                    name='rating'
                    starDimension="24px"
                    starSpacing="4px"
                    />
                </div>    
                <div className="card card-info">
                <div className="card-block">
                    <textarea placeholder="Write your comment here!" className="pb-cmnt-textarea" onChange={this.handleChange} value={this.state.comment}></textarea>
                </div>
                <button className="btn btn-info pull-right" type="button" onClick={this.onClick}>Share</button>
                </div>
                </div>
                </div>

                </Modal>
                
                
            </div>);
        return(
            
            <div className="container mb-5">
            
            {
                comments.map((doc, index)=>{
                    let {firstname, lastname, _id} = doc.userId;
                    this.message = doc.msg;
                    return(

                            <div className="m-10" key={ index }> 
                                <div className="media border p-3">
                                    <img src="https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png" alt="John Doe" className="mr-3 mt-3 rounded-circle" style={{width:'60px'}}/>
                                <div className="media-body">
                                <h4> {firstname} {lastname}</h4>
                                <div className="m-1 disableButton">
                                    { doc.rating===0?'': <StarRatings
                                    rating={doc.rating}
                                    starRatedColor= 'rgba(255,195,0,0.8)'
                                    name='rating'
                                    starDimension="24px"
                                    starSpacing="4px"
                                    />}
                                    
                                </div>

                                <p> {doc.msg===''? '':doc.msg} </p>

                                { this.props.auth.user.id === _id ? updateButton:''}
                                { this.props.auth.isAdmin || this.props.auth.isModerator? <button type="button" className="btn btn-danger ml-2" onClick={()=>{this.onDeleteByAuthority(_id)}}>Delete</button>:''}
                                
                                </div>
                                </div>
                            </div>
        
    
        
                        );
            })

            }

            </div>
            
            

        );
            
            
    }

}

const mapStateToProps = state => ({
    product: state.product,
    auth: state.auth
  });
  
export default connect(mapStateToProps, { setReview, sendUpdatedReview, setAfterRemovingReview })(withRouter(AllComments));


