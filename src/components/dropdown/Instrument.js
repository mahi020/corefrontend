import React, { Component } from 'react';
import { connect } from 'react-redux';
import PaginationComponent from "react-reactstrap-pagination";
import { getDropdownProducts, addActivePage } from '../../actions/productActions';
import { withRouter } from 'react-router-dom';

class Instrument extends Component {
  
    constructor(){

        super();
        this.handleSelected = this.handleSelected.bind(this);
        this.onClicked = this.onClicked.bind(this);
        this.showProducts = this.showProducts.bind(this);
  
      }
      handleSelected(selectedPage) {
          
          this.props.addActivePage(selectedPage);
          this.props.getDropdownProducts(selectedPage, "Instrument");    
      }
      componentDidMount()
      {
          this.props.getDropdownProducts(this.props.product.activePage, "Instrument");
      }
      onClicked(productId){
          //this.props.addAProductToCart(this.props.product.cart, prod);
          console.log("Clicked..");
          this.props.history.push(`/productDetails/${productId}`);
  
      }
      showProducts(prod){
  
          let table = []
          let k=0;
          for (let i = 0; i < prod.length/3; i++) 
          {
              let children = []
              
              for (let j = 0; j < 3; j++) {
                  if(prod[k])
                  {
                      children.push(this.productComponent(prod[k]));
                      k++;
                  }
              }
              table.push(<div className="row" key={ i }> { children } </div>)
          }
          return table
  
      }
      productComponent(child){
  
      
          return (<div className="col-md-4 product mb-3" key={ child._id }>
          <figure className="card shadow card-product" style={{ borderRadius: '1rem' }}>
              <div className="img-wrap"><img src={ child.image } alt={child.title} /></div>
              <figcaption className="info-wrap">
                      <h4 className="title">{ child.title }</h4>
                      <p className="card-text">{ child.desc }</p> 
              </figcaption>
              <div>
                <div className="price-wrap h5 pl-3">
                    <span className="price-new mt-2 mb-2">${ child.price }</span> 
                </div> 
              </div>
              <button onClick={()=>{this.onClicked(child._id)}} className="btn btn-info m-2" style={{ borderRadius: '1rem' }}>See Details</button> 
          </figure>
      </div>);
      }
  
      render() {
          const { allProducts } = this.props.product;
  
          return (
              <div className="container mt-5">
               <br/> <br/> <br/>
                  { this.showProducts(allProducts) }
                  
                      <PaginationComponent
                          totalItems={this.props.product.numOfProd}
                          pageSize={6}
                          onSelect={this.handleSelected}
                          />                
              </div>
              
  
  
          );
      
      }

  
}
const mapStateToProps = state => ({
  product: state.product
});

export default connect(mapStateToProps, { getDropdownProducts, addActivePage })(withRouter(Instrument));