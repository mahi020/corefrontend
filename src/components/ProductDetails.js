import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAProductToShow, addAnotherProductToCart, removeItemFromCart } from '../actions/productActions';
import Rating from './Rating';
import CommentBox from './CommentBox';
import AllComments from './AllComments';

class ProductDetails extends Component {

    constructor(){
        super();
        this.items = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];
        //this.onChange = this.onChange.bind(this);
        this.total = 0;
        this.selected = 1;
        this.onclick = this.onClick.bind(this);
        this.onRemove = this.onRemove.bind(this);
      }
    
      componentDidMount(){
        if(this.props.auth.isAuthenticated)
        {
          this.props.getAProductToShow(this.props.match.params.id, this.props.auth.user.id);
          //this.props.setIncrementedTotal(this.props.product.cart);
        }
        else
        {
          this.props.getAProductToShow(this.props.match.params.id,null);
        }
        
      }
      onChange(prod,e){
    
        
        //this.props.addAProductToCart(this.props.product.cart, prod, e.target.value);
        //console.log(prod, e.target.value);
        this.selected = e.target.value;
        

      }
      
      onClick(prod, selected)
      {
        //this.props.addAProductToCart(this.props.product.cart, prod)
        console.log(prod, parseInt(selected));
        this.props.addAnotherProductToCart(prod, parseInt(selected));
        //console.log(this.props.product.cart);
      }
      onRemove(prod)
      {
        this.props.removeItemFromCart(prod);
      }
  render() {
    
    return (
      <div>
        <br/> <br/> <br/>

      <div className="container mt-5">
		    <div className="row">
            <div className="col-md-5 offset-md-1 p-2 shadow" style={{ backgroundColor:'#FFFFFF' }}>						
						  <div className="tab-pane d-flex justify-content-center align-items-center active">
						        <img src={this.props.product.item.image} />
						  </div>
						
			      </div>    
        

            <div className="col-md-5 p-5 shadow" style={{ backgroundColor:'#79B0DC' }}>
            <h3 className="product-title" style={{ color:'#FFFFFF' }}>{this.props.product.item.title}</h3>
			<div className="rating">
              <Rating/>
			</div>
			<p className="product-description" style={{ color:'#FFFFFF' }}>{this.props.product.item.desc}</p>
			<h4 className="price" style={{ color:'#FFFFFF' }}>Price: <span style={{ color:'#FFFFFF' }}>${this.props.product.item.price}</span></h4>

						<div className="action" style={{ width: "200px" }}>

                            <select className="form-control" defaultValue="1" onChange={(e)=>          {this.onChange(this.props.product.item._id,e)}}>
                                    {this.items.slice(0, this.props.product.item.quantity).map((item, index)=>{

                                    return(<option key={index +''+this.props.product.item._id}>{ item }</option>);

                            })}
                            </select>
							<button className="btn btn-block btn-outline-info mt-3" type="button" style={{ borderRadius:'1.5rem' }} onClick={()=>{ this.onClick(this.props.product.item, this.selected) }}>Add to cart</button>
							

				</div>
            </div>

        </div>
	  </div>
  <div className="container mt-5">
  <div className="row">
      <div className="col-md-10 offset-md-1 ccard shadow">
      
        { this.props.product.comments.length>0?<h3 className="m-3">All Reviews</h3>: <h3>No Reviews</h3> }
        <AllComments/>

        <CommentBox/>
      
      </div>
  </div>
  
  </div>
  </div>
    );
  }
}

const mapStateToProps = state => ({
    product: state.product,
    auth: state.auth
  });
  

export default connect(mapStateToProps,{ getAProductToShow, addAnotherProductToCart, removeItemFromCart})(ProductDetails);
;