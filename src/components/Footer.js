import React from 'react';
import { Link } from 'react-router-dom';

export default () => {

  return (

    <footer className="page-footer font-small blue-grey lighten-5 mb-1 p-5" style={{ background: '#bdbdbd' }}>

    <div className="container text-center text-md-left mt-5">

  
    <div className="row mt-3 dark-grey-text">

   
    <div className="col-md-3 col-lg-4 col-xl-3 mb-4">

   
      <h6 className="text-uppercase font-weight-bold" style={{color:'#FFFFFF'}}>About Us</h6>
      <hr className="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style={{width: '70px', backgroundColor: 'lightcoral'}}/>
      <p>
        <Link to="/about" style={{color:'#FFFFFF'}}> Click Here </Link>
      </p>
      

    </div>
   
    
    <div className="col-md-5 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

    <h5 className="text-uppercase font-weight-bold" style={{color:'#FFFFFF'}}>Contact Us</h5>
      <hr className="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style={{width: '70px', backgroundColor: 'lightcoral'}}/>

       <h6 style={{color:'#FFFFFF'}}>Email: mahihussain@hotmail.com</h6>
       <h6 style={{color:'#FFFFFF'}}>Phone: +8801688732729</h6> 
    </div>

  </div>

</div>

</footer>

  );

};