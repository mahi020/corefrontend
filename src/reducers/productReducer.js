import { ADD_PRODUCT, GET_ALL_PRODUCTS, SET_PRODUCT, SET_A_CART,SET_TOTAL, ADD_ACTIVE_PAGE, SET_A_SINGLE_PRODUCT, ADD_A_PRODUCT_TO_CART, REMOVE_ITEM_FROM_CART, SET_ALREADY_RATED,SET_REVIEW, SET_UPDATED_ITEM , SET_AFTER_REMOVING_REVIEW, SET_SEARCHED_PRODUCTS, SET_DROPDOWN_PRODUCTS, ADD_ANOTHER_PRODUCT_TO_CART} from '../actions/types';

const initialState = {

  all:{},
  allProducts: [],
  cart:[],
  total:0,
  numOfProd:0,
  activePage: 1,
  id:'',
  desc:'',
  price: '',
  quantity:'',
  image:'',
  title:'',
  item:{},
  canRate: false,
  comments:[],
  canUpdate: false,
  isSeller: false,
  message:'',
  rating:0,
  search:'',
  category:''

};

export default function(state = initialState, action){

  switch (action.type) {

    case ADD_PRODUCT:
      return {
                ...state,
                all: action.payload
              };
    case GET_ALL_PRODUCTS:
      return {

      ...state,
      allProducts: action.payload.data,
      numOfProd: action.payload.numOfProd,
      activePage: action.payload.activePage

    };
    
    case SET_PRODUCT:
      return {

        ...state,
        id: action.payload._id,
        price: action.payload.price,
        quantity: action.payload.quantity

      };
    case SET_A_CART:
    let { cart, total } = action.payload;
    //console.log(cart);
    return {
      ...state,
      cart: cart,
      total: total
    };
    case ADD_ACTIVE_PAGE:
    return {

      ...state,
      activePage: action.payload

    };
    case SET_A_SINGLE_PRODUCT:
    console.log(action.payload.canRate);
    console.log(action.payload.comments);
    console.log(action.payload);
    return{
      ...state,
      id: action.payload.data._id,
      image: action.payload.data.image,
      desc: action.payload.data.desc,
      quantity: action.payload.data.quantity,
      price: action.payload.data.price,
      title:action.payload.data.title,
      item: action.payload.data,
      canRate: action.payload.canRate,
      canUpdate: action.payload.canUpdate,
      isSeller: action.payload.isSeller,
      comments: action.payload.comments
    };
    case ADD_A_PRODUCT_TO_CART:
      
      let addedItem = action.payload.prod;
      //console.log(addedItem);
      let existed_item = state.cart.find(item=> addedItem._id === item._id);
      //console.log(existed_item);
      if(existed_item)
        {
            //console.log(addedItem);
            //console.log([...state.cart]);
            //let new_items = state.cart.filter(item=> addedItem._id !== item._id);
            let updateTotal = state.total - (existed_item.price*existed_item.qty);
            //console.log(updateTotal);
            addedItem.qty = action.payload.qty;
            //console.log(addedItem);
            let newTotal = addedItem.price*addedItem.qty;
            //console.log(newTotal);
            let t = updateTotal + newTotal;
            console.log(t);
            let newObj = {
              ...state,
              cart: [...state.cart],
              total : t
            };
            let { cart, total } = newObj;
            localStorage.setItem('cart',JSON.stringify(cart));
            localStorage.setItem('total',total);
            return newObj;
        }
      else
         {
            console.log(addedItem);
            //console.log("Lol");
            //console.log(addedItem.price*parseInt(action.payload.qty));
            //console.log(state.cart);
            addedItem.qty = action.payload.qty;
            //calculating the total
            let newTotal = state.total + (addedItem.price*addedItem.qty); 
            console.log(newTotal);
            console.log([...state.cart, addedItem]);
            let newObj = {
              ...state,
              cart: [...state.cart, addedItem],
              total : newTotal
            };

            let { cart, total } = newObj;
            localStorage.setItem('cart',JSON.stringify(cart));
            localStorage.setItem('total',total);

            return newObj; 
            
        }
    case ADD_ANOTHER_PRODUCT_TO_CART:
      let added = action.payload.prod;
      console.log(added);
      let existed = state.cart.find(item=> added._id === item._id);
      console.log(existed);
      if(existed)
        {
            //console.log(addedItem);
            //console.log([...state.cart]);
            let new_items = state.cart.filter(item=> added._id !== item._id);
            let updateTotal = state.total - (existed.price*existed.qty);
            console.log(updateTotal);
            added.qty = action.payload.qty;
            console.log(added);
            let newTotal = added.price*added.qty;
            console.log(newTotal);
            let t = updateTotal + newTotal;
            console.log(t);
            let newObj = {
              ...state,
              cart: [...new_items, added],
              total : t
            };
            let { cart, total } = newObj;
            localStorage.setItem('cart',JSON.stringify(cart));
            localStorage.setItem('total',total);
            return newObj;
        }
      else
         {
            console.log(added);
            //console.log("Lol");
            //console.log(addedItem.price*parseInt(action.payload.qty));
            //console.log(state.cart);
            added.qty = action.payload.qty;
            //calculating the total
            let newTotal = state.total + (added.price*added.qty); 
            console.log(newTotal);
            console.log([...state.cart, added]);
            let newObj = {
              ...state,
              cart: [...state.cart, added],
              total : newTotal
            };

            let { cart, total } = newObj;
            localStorage.setItem('cart',JSON.stringify(cart));
            localStorage.setItem('total',total);

            return newObj; 
            
        }
    case REMOVE_ITEM_FROM_CART:

        let itemToRemove= state.cart.find(item=> action.payload._id === item._id);
        let newTotal = state.total - (itemToRemove.price * itemToRemove.qty );
        let new_items = state.cart.filter(item=> action.payload._id !== item._id);
        
        //calculating the total
        
        console.log(itemToRemove);
        console.log(new_items);
        localStorage.setItem('cart',JSON.stringify(new_items));
        localStorage.setItem('total',newTotal);

        if(state.cart.length==0)
        {
          localStorage.removeItem('cart');
          localStorage.removeItem('total');
        }
            
        return{
            ...state,
            cart: new_items,
            total: newTotal
        };
    case SET_ALREADY_RATED:
    console.log(action.payload);
    return{
      ...state,
      canRate: action.payload,

    };
    case SET_REVIEW:
    console.log(action.payload.message);
    console.log(action.payload.rating);
    return{
      ...state,
      message: action.payload.message,
      rating: action.payload.rating

    };
    case SET_UPDATED_ITEM:
    return{
      ...state,
      item: action.payload
    };
    case SET_AFTER_REMOVING_REVIEW:
    return{

      ...state,
      canRate: action.payload

    };
    case SET_SEARCHED_PRODUCTS:
    return{

      ...state,
      search: action.payload
    };
    case SET_DROPDOWN_PRODUCTS:
    return{

      ...state,
      category: action.payload

    };
    default:
      return state;

  }
}
