import { SET_CURRENT_USER, SET_A_USERS_ORDER, SET_A_USER_PRODUCTS, SET_A_USER_PRODUCT, SET_RECOVERY, GET_SHIPPING } from '../actions/types';
import isEmpty from '../validation/isEmpty';
import checker from '../utils/checker';

const initialState = {

    isAuthenticated: false,
    isAdmin: false,
    isModerator: false,
    user: {},
    order: [],
    userProducts:[],
    singleProduct:[]

};

export default function(state = initialState, action){

  switch (action.type) {

    case SET_CURRENT_USER:
    return{
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        isAdmin: checker(action.payload.isAdmin),
        isModerator: checker(action.payload.isModerator),
        user: action.payload
    };
    case SET_A_USERS_ORDER:
    return{
      ...state,
      order: action.payload
    };
    case SET_A_USER_PRODUCTS:
    return{
      ...state,
      userProducts: action.payload
    }
    case SET_A_USER_PRODUCT:
    return{
      ...state,
      singleProduct: action.payload
    }
    case SET_RECOVERY:
    return action.payload;

    case GET_SHIPPING:
    return {...state, ...action.payload}
    
    default:
      return state;

  }
}
