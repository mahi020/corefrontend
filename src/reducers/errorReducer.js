import { GET_ERRORS, SET_ADD_PRODUCT_ERRORS, SET_UPDATE_PRODUCT_ERRORS, SET_SHIPPING_ERROR} from '../actions/types';

const initialState = {};

export default function(state = initialState, action){

  switch (action.type) {

    case GET_ERRORS:
      //console.log(action.payload);
      return action.payload;
    case SET_ADD_PRODUCT_ERRORS:
      return action.payload;
    case SET_UPDATE_PRODUCT_ERRORS:
      return action.payload;
    case SET_SHIPPING_ERROR:
      return action.payload;
    default:
      return state;

  }
}
