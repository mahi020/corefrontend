import { SET_ALL_ORDERS, SET_ALL_USERS, SET_ALL_PRODUCTS, SET_A_SINGLE_PRODUCT_PRIVATE } from '../actions/types';

const initialState = {

    allOrders:[],
    allUsers:[],
    allProducts:[],
    singleProduct:{}
};

export default function(state = initialState, action){

  switch (action.type) {
    case SET_ALL_ORDERS:
    console.log(action.payload);
    return{
        ...state,
        allOrders: action.payload
    };
    case SET_ALL_USERS:
    return{
        ...state,
        allUsers: action.payload
    };
    case SET_ALL_PRODUCTS:
    return{
        ...state,
        allProducts: action.payload
    };
    case SET_A_SINGLE_PRODUCT_PRIVATE:
    return{
        ...state,
        singleProduct: action.payload
    };
    default:
      return state;

  }
}
