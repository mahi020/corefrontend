const Validator = require('validator');
const isEmpty = require('./isEmpty');

export const addProductValidate = (data)=>{
  let errors = {};
  
  //console.log(typeof(data.image));

  data.title = !isEmpty(data.title) ? data.title : '';
  data.image = !isEmpty(data.image) ? data.image : '';
  data.desc = !isEmpty(data.desc) ? data.desc : '';
  data.price = !isEmpty(data.price) ? data.price : '';
  data.quantity = !isEmpty(data.quantity) ? data.quantity : '';
  data.author = !isEmpty(data.author) ? data.author : '';

  
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Title field is required';
  }
  if (Validator.isEmpty(data.image)) {
    errors.image = 'Image field is required';
  }
  if (Validator.isEmpty(data.desc)) {
    errors.desc = 'Description field is required';
  }
  if (Validator.isEmpty(data.price)) {
    errors.price = 'price field is required';
  }
  if (Validator.isEmpty(data.quantity)) {
    errors.quantity = 'Quantity field is required';
  }
  if (Validator.isEmpty(data.author)) {
    errors.author = 'Author field is required';
  }

  let a = data.image.endsWith('.jpg');
  let b = data.image.endsWith('.png');
  let c = data.image.endsWith('.jpeg');
  let d = data.image.endsWith('.gif'); 
  if(a===false && b===false && c===false && d===false)
  {
    errors.image = 'Please Upload An Image File';
  }
  
  
  if(Validator.isAlpha(data.price))
  {
      errors.price = 'Price Should be Number Only';
  }
  if(data.price<=0)
  {
    errors.price = data.price+' is not Valid';
  }
  if(Validator.isAlpha(data.quantity))
  {
      errors.quantity = 'Quantity Should be Number Only';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
