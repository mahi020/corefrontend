const isEmpty = require('./isEmpty');

export const updateProductValidate = (data)=>{
  let errors = {};
  
  //console.log(data);

  data.price = !isEmpty(data.price) ? data.price : '';
  data.quantity = !isEmpty(data.quantity) ? data.quantity : '';
  //console.log(data.price);
  //console.log(data.quantity);
  
  if (data.price=='') {
    errors.price = 'price field is required';
  }
  if (data.quantity=='') {
    errors.quantity = 'Quantity field is required';
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
