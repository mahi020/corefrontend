import React, { Component, lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/popper.js/dist/umd/popper.min.js';
import '../node_modules/jquery/dist/jquery.min.js';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../node_modules/font-awesome/css/font-awesome.min.css';

import PrivateRoute from './components/common/PrivateRoute';
import AdminRoute from './components/common/AdminRoute';
import ModeratorRoute from './components/common/ModeratorRoute';

//import Landing from './components/Landing';
import { Provider } from 'react-redux';
import store from './store';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, checkAccessToken, setAnonymousToken } from './actions/authActions';
import { logoutUser } from './actions/authActions';
//import { addInCart } from './actions/productActions';
//import ProductsFromDropdown from './components/ProductsFromDropdown';
//import { Redirect } from "react-router-dom";
//import axios from 'axios';
import './App.css';
import { addCart } from './actions/productActions';

const Navbar = lazy(()=> import('./components/Navbar'));
const Register = lazy(()=> import ('./components/Register'));
const Login = lazy(()=> import('./components/Login'));
const ProductView = lazy(()=> import('./components/ProductView'));
const TeamDesign = lazy(()=> import('./components/TeamDesign'));
const Cart2 = lazy(()=> import('./components/Cart2'));
const LoginVerify = lazy(()=> import('./components/LoginVerify'));
const Electronics = lazy(()=> import('./components/dropdown/Electronics'));
const Clothing = lazy(()=> import('./components/dropdown/Clothing'));
const Instrument = lazy(()=> import('./components/dropdown/Instrument'));
const Books = lazy(()=> import('./components/dropdown/Books'));
const Other = lazy(()=> import('./components/dropdown/Other'));
const Rating = lazy(()=> import('./components/Rating'));
const ProductDetails = lazy(()=> import('./components/ProductDetails'));
const ProductsFromSearch = lazy(()=> import('./components/ProductsFromSearch'));
const NotFound = lazy(()=> import('./components/NotFound'));
const AccountRecovery = lazy(()=> import('./components/AccountRecovery'));

const UserOrder = lazy(()=> import('./components/user/AllOrders'));
const AddProduct = lazy(()=>import('./components/user/AddProduct'));
const ShowUserProduct = lazy(()=> import('./components/user/ShowAProduct'));
const AllUsersProduct = lazy(()=> import('./components/user/AllProducts'));
const UpdateProduct = lazy(()=> import('./components/user/UpdateProduct'));
const Me = lazy(()=> import('./components/user/Me'));

const ModeratorMe = lazy(()=> import('./components/moderator/Me')); 
const AllOrders = lazy(()=> import('./components/moderator/AllOrders'));
const AllUsers = lazy(()=> import('./components/moderator/AllUsers'));
const AllProducts = lazy(()=> import('./components/moderator/AllProducts'));
const ShowAProduct = lazy(()=> import('./components/moderator/ShowAProduct'));
const RedirectRoute =lazy(()=> import('./components/common/RedirectRoute')); 

const Admin = lazy(()=> import('./components/admin/Me'));
const AdminAllOrders = lazy(()=> import('./components/admin/AllOrders'));
const AdminAllProducts = lazy(()=> import('./components/admin/AllProducts'));
const AdminAllUsers = lazy(()=> import('./components/admin/AllUsers'));
const AdminShowAProduct = lazy(()=> import('./components/admin/ShowAProduct'));

if(localStorage.cart)
{
  store.dispatch(addCart(localStorage.cart));
}

if(!localStorage.jwtRefreshToken)
{
  store.dispatch(setAnonymousToken());
}
if(localStorage.jwtRefreshToken)
{
  if(localStorage.jwtAnonymousToken)
  {
    localStorage.removeItem('jwtAnonymousToken');
  }
  setAuthToken(localStorage.jwtToken);
  let decodeJwt = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decodeJwt));

  //console.log("Wrong");
  let refreshToken = localStorage.jwtRefreshToken;
  let decoded = jwt_decode(refreshToken);

  let currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {

    store.dispatch(logoutUser());
    window.location.href = '/login';
  }
  else
  {
      store.dispatch(checkAccessToken(currentTime, refreshToken));
  }

}





/*axios.interceptors.request.use(request =>{

  if(localStorage.jwtRefreshToken)
  {
    //console.log("Wrong");
    let refreshToken = localStorage.jwtRefreshToken;
    let decoded = jwt_decode(refreshToken);
  
    let currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
  
      store.dispatch(logoutUser());
      window.location.href = '/login';
    }
    else
    {
        store.dispatch(checkAccessToken(currentTime, refreshToken));
    }
  
  }
  
  return request;

},error =>{

    return Promise.reject(error);

});
*/

class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <Router>
        <div className="App">
        <Route component = {waitingComponent(Navbar)} />
        <Switch>
          <Route exact path="/" component={waitingComponent(ProductView)}/>

            <Route exact path="/register" component={waitingComponent(Register)} />
            <Route exact path="/login/:id" component={waitingComponent(LoginVerify)}/>
            <Route exact path="/login" component={waitingComponent(Login)}/>
            <Route exact path="/accountRecovery" component={waitingComponent(AccountRecovery)}/>
            <Route exact path="/about" component={waitingComponent(TeamDesign)}/>
            <Route exact path="/cart" component={ waitingComponent(Cart2)}/>

            <Route exact path="/dropdown/Clothing" component={ waitingComponent(Clothing) }/>
            <Route exact path="/dropdown/Instrument" component={ waitingComponent(Instrument) }/>
            <Route exact path="/dropdown/Electronics" component={ waitingComponent(Electronics) }/>
            <Route exact path="/dropdown/Books" component={ waitingComponent(Books) }/>
            <Route exact path="/dropdown/Other" component={ waitingComponent(Other) }/>

            <Route exact path="/search" component={ waitingComponent(ProductsFromSearch) }/>
            <Route exact path="/rating" component={ waitingComponent(Rating)}/>
            <Route exact path="/productDetails/:id" component={ waitingComponent(ProductDetails) }/>
            <Route exact path="/redirectRoute" component={ waitingComponent(RedirectRoute) }/>
        </Switch>
          
        


            <Switch>
              <PrivateRoute exact path="/me" component={waitingComponent(Me)} />
            </Switch>
            <Switch>
              <PrivateRoute exact path="/addProduct" component={waitingComponent(AddProduct)} />
            </Switch>
            <Switch>
              <PrivateRoute exact path="/updateProduct/:id" component={waitingComponent(UpdateProduct)} />
            </Switch>
            <Switch>
              <PrivateRoute exact path="/userOrder" component={waitingComponent(UserOrder)} />
            </Switch>
            <Switch>
              <PrivateRoute exact path="/myProducts" component={waitingComponent(AllUsersProduct)} />
            </Switch>
            <Switch>
              <PrivateRoute exact path="/showUserProduct/:id" component = { waitingComponent(ShowUserProduct) }/>
            </Switch>
               
            

            

            
            <Switch>
              <ModeratorRoute exact path="/moderator/me" component={waitingComponent(ModeratorMe)} />
            </Switch>
            <Switch>
              <ModeratorRoute exact path="/home" component={ waitingComponent(AllOrders)}/>
            </Switch>
            <Switch>
              <ModeratorRoute exact path="/allUsers" component={ waitingComponent(AllUsers) }/>
            </Switch>
            <Switch>
              <ModeratorRoute exact path="/allProducts" component={ waitingComponent(AllProducts) }/>
            </Switch>
            <Switch>
              <ModeratorRoute exact path="/showAProduct/:id" component = { waitingComponent(ShowAProduct) }/>
            </Switch>   
            


            <Switch>
              <AdminRoute exact path="/admin/me" component={waitingComponent(Admin)} />
            </Switch>
            <Switch>
              <AdminRoute exact path="/admin/home" component={waitingComponent(AdminAllOrders)} />
            </Switch>
            <Switch>
              <AdminRoute exact path="/admin/allProducts" component={waitingComponent(AdminAllProducts)} />
            </Switch>
            <Switch>
              <AdminRoute exact path="/admin/allUsers" component={waitingComponent(AdminAllUsers)}/>
            </Switch>
            <Switch>
              <AdminRoute exact path="/admin/showAProduct/:id" component={waitingComponent(AdminShowAProduct)} />
            </Switch>

            

        </div>
      </Router>
      </Provider>
    );
  }
}

function waitingComponent(Component)
{
    return props=>(

      <Suspense fallback={ <div> Loading ... </div> }>
        <Component { ...props }/>
      </Suspense>

    );
}
export default App;
