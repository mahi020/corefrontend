import { GET_ERRORS, SET_CURRENT_USER, SET_A_USERS_ORDER, SET_A_USER_PRODUCTS , SET_A_USER_PRODUCT, SET_RECOVERY, GET_SHIPPING, SET_SHIPPING_ERROR} from './types';
import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import isEmpty from '../validation/isEmpty';
import Validator from 'validator';

export const registerUser = (userData, history) => dispatch =>{

  axios
    .post('/user/register', userData)
    .then(res => history.push('/login'))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }) );

};

export const loginUser = (userData, history) => dispatch =>{

  axios.post('/user/login', userData).then(res =>{

    //console.log(userData);
    //console.log(res);
    let { token } = res.data;
    let { refreshToken } = res.data;
    localStorage.setItem('jwtToken', token);
    setAuthToken(token);
    let decoded = jwt_decode(token);
    localStorage.setItem('jwtRefreshToken', refreshToken);
    console.log(decoded);
    console.log("Token: "+token);
    console.log("Refresh Token: "+refreshToken);

    //let cur = Date.now()/1000;
    //console.log("Current Time:"+cur);
    dispatch(setCurrentUser(decoded));
    history.push('/');

  }).catch(err =>{
    //console.log("This is The error "+err.response.data);
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
  })});
}

export const logoutUser = () => dispatch =>{

    localStorage.removeItem('jwtToken');
    localStorage.removeItem('jwtRefreshToken');
    setAuthToken(false);
    dispatch(setCurrentUser({}));


}

export const verifyUser = (id, history) => dispatch=>{

  console.log(id);
  let newId = {
    id: id
  }
  axios.post('/user/verify',newId).then(res=>{

    console.log(res);
    //alert("Please Complete Email Verification Before Login!!");
    history.push('/login');

  }).catch(err=>{

    console.log(err);
    history.push('/login');

  });

};
export const checkAccessToken = (currentTime, refreshToken) => dispatch => {

  let token = localStorage.jwtToken;
  let decodedAccess = jwt_decode(token);

  if (decodedAccess.exp < currentTime) {

    let { email } = decodedAccess;
    let userData = {

          "email": email,
          "refreshToken": refreshToken
    };

    axios.post('/user/token',userData).then(res =>{

      let { token } = res.data;
      let { refreshToken } = res.data;
      localStorage.setItem('jwtToken', token);
      setAuthToken(token);
      console.log("Token Updated: "+token);
      console.log("Refresh Token: "+refreshToken);


    }).catch(err=>{
          console.log("Something went Wrong in the Backend "+err);
    });
  }


}

export const setAnonymousToken = () => dispatch =>{

  axios.get('/user/anonymous').then(res =>{

    let { token } = res.data;
    localStorage.setItem('jwtAnonymousToken', token);
    setAuthToken(token);
    //let jwtData = jwt_decode(token);
    //console.log(jwtData);
    console.log("Anonymous Token: "+token);
    //console.log(res);

  }).catch(err=>{
        console.log("Something went Wrong in the Backend "+err);
  });

}

export const getUserOrder = (id) => dispatch=>{

  let newObj = {
      id
  };

  axios.post('/orders/userOrder', newObj).then(res=>{

      dispatch({

        type: SET_A_USERS_ORDER,
        payload: res.data

      });

  }).catch(err=>{

      console.log(err);

    });

};


export const getUserProducts = (id) => dispatch=>{

  let newObj = {
      id
  };

  axios.post('/products/userProducts', newObj).then(res=>{

      dispatch({

        type: SET_A_USER_PRODUCTS,
        payload: res.data

      });

  }).catch(err=>{

    console.log(err);

  });

};

export const getAUserProduct = (id)=> dispatch=>{

  console.log("Here"+id);
  axios.get(`/products/showAProduct/${id}`).then(res=>{
    //console.log(res.data);
    dispatch({
        type: SET_A_USER_PRODUCT,
        payload: res.data
    });
}).catch(err=>{

    console.log("There is Some Error while Fetching Product data from Database "+err);

});

};


export const sendRecoveryEmail = (email) => dispatch=>{

  axios
    .post('/user/findRecoveryEmail', email)
    .then(res =>{
      console.log(res);
      dispatch(setRecovery(res.data))
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }) );

}

export const sendCode = (userData) => dispatch=>{

  axios
    .post('/user/verifyRecoveryCode', userData)
    .then(res =>{
      console.log(res);
      dispatch(setRecovery(res.data))
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }) );


}

export const sendNewPassword = (userData) => dispatch=>{

  axios
    .post('/user/setNewPassword', userData)
    .then(res =>{
      console.log(res);
      dispatch(setRecovery(res.data))
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }) );


}
export const setRecovery = (data) =>{

  return {
    type:SET_RECOVERY,
    payload: data

  };

}

export const getShipping = (id) => dispatch=>{

  let newObj = {
    id: id
  };
  axios
    .post('/user/shippingAddress', newObj)
    .then(res =>{
      console.log(res);
      dispatch({
        type: GET_SHIPPING,
        payload: res.data
      });
    })
    .catch(err =>{
        console.log(err);  
    });


}

export const setShippingError = (data) => dispatch=>{

    let errors = {};

    if (Validator.isEmpty(data.address)) {
      errors.address = 'Address field is required';
    }
    if (Validator.isEmpty(data.country)) {
      errors.country = 'Country field is required';
    }
    if (Validator.isEmpty(data.zip)) {
      errors.zip = 'Zip Code field is required';
    }
  
    dispatch({

      type: SET_SHIPPING_ERROR,
      payload: errors

    });
}



export const setCurrentUser = (decoded) =>{

      return {
          type: SET_CURRENT_USER,
          payload: decoded
      };

}
