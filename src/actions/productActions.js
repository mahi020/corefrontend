import axios from 'axios';
import { ADD_PRODUCT, GET_ALL_PRODUCTS, SET_PRODUCT, SET_A_CART, ADD_ACTIVE_PAGE, SET_A_SINGLE_PRODUCT, ADD_A_PRODUCT_TO_CART, REMOVE_ITEM_FROM_CART, SET_ALREADY_RATED, SET_REVIEW,SET_UPDATED_ITEM, SET_AFTER_REMOVING_REVIEW, SET_SEARCHED_PRODUCTS, SET_ADD_PRODUCT_ERRORS, SET_UPDATE_PRODUCT_ERRORS, SET_DROPDOWN_PRODUCTS, ADD_ANOTHER_PRODUCT_TO_CART } from '../actions/types';
import { addProductValidate } from '../validation/addProductValidate';
import { updateProductValidate } from '../validation/updateProductValidate';
import Swal from 'sweetalert2';

export const addProduct = (newProduct, product,history) => dispatch =>{
  
    //console.log(product);
    const { errors, isValid } = addProductValidate(product);
    //console.log(isValid);
    //console.log(errors);
  //dispatch(printValue(newProduct));
  console.log(product);
  if(isValid)
  {
    axios
    .post('/products/add', newProduct, { headers: {  'Content-Type': 'multipart/form-data' } })
    .then(res => {

        
        dispatch({
                type:ADD_PRODUCT,
                payload: res.data
        });
        Swal.fire(
            'Your Has been Added',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/me');
          },1000);


    })
    .catch(err =>{

        Promise.reject(new Error('Failed!!'));  
    });
  
  }
  else
  {
    //console.log(errors);
    dispatch({
        type: SET_ADD_PRODUCT_ERRORS,
        payload: errors
    });

  }
    

};

export const getAllProducts = (newPage=1) => dispatch =>{
 
    let page = {
        newPage
    };
    //console.log("New Page "+newPage);
    //console.log(page);
    axios
    .post('/products', page)
    .then(res => {
        console.log(res.data);
        dispatch({
                type: GET_ALL_PRODUCTS,
                payload: res.data
        });

    })
    .catch(err =>{

        Promise.reject(new Error('Failed!!'));
    });


};
export const setDropdownProducts = (category, history) => dispatch=>{

    console.log("Set Dropdown");
    dispatch({

        type: SET_DROPDOWN_PRODUCTS,
        payload: category

    });

};

export const getDropdownProducts = (newPage=1, category) => dispatch =>{
 
    let page = {
        newPage,
        category
    };
    //console.log("New Page "+newPage);
    //console.log(page);
    axios
    .post('/products/dropdown', page)
    .then(res => {
        console.log(res.data);
        dispatch({
                type: GET_ALL_PRODUCTS,
                payload: res.data
        });


    })
    .catch(err =>{

        Promise.reject(new Error('Failed!!'));
    });


};

export const setSearchedProducts = (search, history) => dispatch=>{

    dispatch({

        type: SET_SEARCHED_PRODUCTS,
        payload: search

    });
    history.push('/search');

};

export const getSearchedProducts = (newPage=1,search) => dispatch =>{

    let page = {
        search,
        newPage
    };
    //console.log("New Page "+newPage);
    //console.log(page);
    axios
    .post('/products/search', page)
    .then(res => {
        console.log(res.data);
        dispatch({
                type: GET_ALL_PRODUCTS,
                payload: res.data
        });

    })
    .catch(err =>{

        Promise.reject(new Error('Failed!!'));
    });



};

export const getAProduct = (productId) => dispatch =>{
  
    //dispatch(printValue(newProduct));
    axios.get(`/products/edit/${productId}`)
      .then(res => {
  
          dispatch({

                  type: SET_PRODUCT,
                  payload: res.data
          });
  
      })
      .catch(err =>{
  
          Promise.reject(new Error('Failed Fetching Data!!'));
      });
  
  };

  
  export const updateProduct = (newProduct, productId, history) => dispatch =>{
  
    //dispatch(printValue(newProduct));
    const { errors, isValid } = updateProductValidate(newProduct);
    if(isValid)
    {
        console.log("here");
       axios
      .post(`/products/update/${productId}`, newProduct)
      .then(res => {

        console.log(res.data);
        Swal.fire(
            'Your Product Has been Updated',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/me');
          },1000);

        
      })
      .catch(err =>{
  
          Promise.reject(new Error('Failed!!'));
      });
    }
    else
    {
        console.log(errors);
        dispatch({
            type: SET_UPDATE_PRODUCT_ERRORS,
            payload: errors
        });   
    }
    
  
};

export const sendToLogin = history =>dispatch=>{

    history.push('/login');

};

export const addActivePage = (page) => dispatch=>{


        dispatch({
            
            type: ADD_ACTIVE_PAGE,
            payload: page
        });
}


export const deleteAProduct = (id, history) => dispatch=>{

    let newObj = {
        id
    }
    

    axios.post('/products/deleteAProduct', newObj).then(res=>{

        console.log(res.data);
        Swal.fire(
            'Product Deleted Successfully',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/me');
          },1000);

    }).catch(err=>{
        console.log(err);
    });


}



// Cart Functions..

export const addAProductToCart = (prod, q) => dispatch =>{

    let qty = parseInt(q);
    console.log(q);
    console.log(prod);    
    let newObj = {

        prod: prod,
        qty: qty
    };
    console.log(newObj);
    dispatch({

        type: ADD_A_PRODUCT_TO_CART,
        payload: newObj

    });


};

export const addAnotherProductToCart = (prod, q) => dispatch =>{

    let qty = parseInt(q);
    console.log(q);
    console.log(prod);    
    let newObj = {

        prod: prod,
        qty: qty
    };
    console.log(newObj);
    dispatch({

        type: ADD_ANOTHER_PRODUCT_TO_CART,
        payload: newObj

    });


};

export const removeItemFromCart = (prod) => dispatch=>{

    dispatch({
        type: REMOVE_ITEM_FROM_CART,
        payload: prod
    });

};

export const addCart = (cartData) => dispatch=>{

    let cart = JSON.parse(cartData);
    let total = localStorage.getItem('total');
    total = parseInt(total);
    let newObj = {

        cart: cart,
        total: total

    };
    //console.log(newObj);
    dispatch({

        type: SET_A_CART,
        payload: newObj

    });
};

// Review And Rating

export const getAProductToShow = (productId, userId) => dispatch=>{

    //dispatch(printValue(newProduct));
    if(!userId)
    {
        userId = null;
    }
    axios.get('/products/showSingleProduct',{

        params: {
            productId: productId,
            userId: userId
     
          }

    })
      .then(res => {
  
          dispatch({

                  type: SET_A_SINGLE_PRODUCT,
                  payload: res.data
          });
          

      })
      .catch(err =>{
  
          Promise.reject(new Error('Failed Fetching Data!!'));
      });
  
};



export const setRating = (productId, newRating, newComment, userId, history)=> dispatch=>{

    console.log(newComment);
    let newObj = {
        rating: newRating,
        userId: userId,
        comment: newComment
    }
    axios
      .post(`/products/review/${productId}`, newObj)
      .then(res => {

        console.log(res.data);
        dispatch({

            type: SET_ALREADY_RATED,
            payload: res.data

        });

        Swal.fire(
            'Your Review Added',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/');
          },1000);


        
      })
      .catch(err =>{
  
          Promise.reject(new Error('Failed!!'));
      });

};



export const setReview = (userId, productId)=>dispatch=>{

    axios.get('/products/review',{

        params: {
            
            userId: userId,
            productId: productId

          }

    })
      .then(res => {
          
        dispatch({
            type: SET_REVIEW,
            payload: res.data 
        });
        
            
      })
      .catch(function (error) {
          console.log(error);
      })

};

export const sendUpdatedReview = (newRating, newComment, userId, productId, history) => dispatch=>{

    let newObj = {
        newRating,
        newComment,
        userId,
        productId
    }
    console.log(newObj)
    axios.post('/products/updatedReview', newObj).then(res=>{

        console.log(res.data);
        dispatch({

            type: SET_UPDATED_ITEM,
            payload: res.data
            
        });
        Swal.fire(
            'Your Review is Updated',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/');
          },1000);



    }).catch(err=>{


    });

};

export const setAfterRemovingReview = (userId, productId, history) =>dispatch=>{

    let newObj = {
        userId,
        productId
    }

    axios.post('/products/removeReview', newObj).then(res=>{

        console.log(res.data);
        dispatch({
            type: SET_AFTER_REMOVING_REVIEW,
            payload: res.data
        });

        Swal.fire(
            'Your Review Has been Deleted',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/');
          },1000);


    }).catch(err=>{
        console.log(err);
    });


}



