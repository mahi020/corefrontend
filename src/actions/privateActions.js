import { SET_ALL_ORDERS, SET_ALL_USERS, SET_ALL_PRODUCTS, SET_A_SINGLE_PRODUCT_PRIVATE } from './types';
import axios from 'axios';
import Swal from 'sweetalert2';

export const getAllOrders = () => dispatch => {

    axios.get('/orders').then(res=>{
        //console.log(res.data);
        dispatch({
            type: SET_ALL_ORDERS,
            payload: res.data
        });
    }).catch(err=>{

        console.log("There is Some Error while Fetching Order data from Database "+err);

    });

};

export const getAllUsers = () => dispatch => {

    axios.get('/user').then(res=>{
        //console.log(res.data);
        dispatch({
            type: SET_ALL_USERS,
            payload: res.data
        });

    }).catch(err=>{

        console.log("There is Some Error while Fetching User data from Database "+err);

    });

};

export const getAllProducts = () => dispatch => {

    axios.get('/products').then(res=>{
        console.log(res.data);
        dispatch({
            type: SET_ALL_PRODUCTS,
            payload: res.data
        });
    }).catch(err=>{

        console.log("There is Some Error while Fetching Product data from Database "+err);

    });

};

export const getASingleProduct = (id) => dispatch => {

    axios.get(`/products/showAProduct/${id}`).then(res=>{
        console.log(res.data);
        dispatch({
            type: SET_A_SINGLE_PRODUCT_PRIVATE,
            payload: res.data
        });
    }).catch(err=>{

        console.log("There is Some Error while Fetching Product data from Database "+err);

    });

};
export const setApproved = (id, history) => dispatch => {

    let newObj = {
        id
    };
    axios.post('/products/approved',newObj).then(res=>{
        console.log(res.data);
        Swal.fire(
            'Product Approved',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/allProducts');
          },2000);


    }).catch(err=>{

        console.log("There is Some Error while Fetching Product data from Database "+err);

    });

};
export const setModerator = (id, history) => dispatch=>{

    let newObj = {

        id

    };
    axios.post('/user/makeModerator', newObj).then(res=>{

        console.log(res.data);
        Swal.fire(
            'Moderator Added',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/admin/me');
          },2000);
                
    }).catch(err=>{

        console.log(err);

    });

};


export const removeFromModerator = (id, history) => dispatch=>{

    let newObj = {

        id

    };
    //console.log(id);
    axios.post('/user/removeModerator', newObj).then(res=>{

        console.log(res.data);
        Swal.fire(
            'Moderator Removed',
            'Have a Good Day!',
            'success'
          );
          setTimeout(function(){
            history.push('/admin/me');
          },2000);
                
    }).catch(err=>{

        console.log(err);

    });

};

export const togglePendingStatus = (data,history) => dispatch=>{

    axios.post('/orders/togglePendingStatus', data).then(res=>{

        //console.log(res.data);
        //console.log("pending");
        history.push('/redirectRoute');
                
    }).catch(err=>{

        console.log(err);

    });


};

export const toggleDeliveryStatus = (data, history) => dispatch=>{

    axios.post('/orders/toggleDeliveryStatus', data).then(res=>{

        //console.log(res.data);
        //console.log("Delivered");
        history.push('/redirectRoute');
                
    }).catch(err=>{

        console.log(err);

    });


};

export const toggleReceivedStatus = (data, history) => dispatch=>{

    axios.post('/orders/toggleReceivedStatus', data).then(res=>{

        //console.log(res.data);
        //console.log("Received");
        history.push('/redirectRoute');
                
    }).catch(err=>{

        console.log(err);

    });


};

