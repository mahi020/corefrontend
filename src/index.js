import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
//import * as serviceWorker from './serviceWorker';

//import axios from 'axios';
//import jwt_decode from 'jwt-decode';
//import store from './store';
//import { logoutUser } from './actions/authActions';
//import setAuthToken from './utils/setAuthToken';



ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
//serviceWorker.unregister();
